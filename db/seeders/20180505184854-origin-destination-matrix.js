module.exports = {
  up: queryInterface => queryInterface.bulkInsert('OriginDestinationMatrices', [{
    origin: 'AC',
    destination: 'AC',
    tariff: 4,
  },
  {
    origin: 'AC',
    destination: 'AL',
    tariff: 6,
  },
  {
    origin: 'AC',
    destination: 'AM',
    tariff: 3,
  },
  {
    origin: 'AC',
    destination: 'AP',
    tariff: 5,
  },
  {
    origin: 'AC',
    destination: 'BA',
    tariff: 6,
  },
  {
    origin: 'AC',
    destination: 'CE',
    tariff: 6,
  },
  {
    origin: 'AC',
    destination: 'DF',
    tariff: 5,
  },
  {
    origin: 'AC',
    destination: 'ES',
    tariff: 6,
  },
  {
    origin: 'AC',
    destination: 'GO',
    tariff: 5,
  },
  {
    origin: 'AC',
    destination: 'MA',
    tariff: 5,
  },
  {
    origin: 'AC',
    destination: 'MG',
    tariff: 5,
  },
  {
    origin: 'AC',
    destination: 'MS',
    tariff: 3,
  },
  {
    origin: 'AC',
    destination: 'MT',
    tariff: 4,
  },
  {
    origin: 'AC',
    destination: 'PA',
    tariff: 5,
  },
  {
    origin: 'AC',
    destination: 'PB',
    tariff: 6,
  },
  {
    origin: 'AC',
    destination: 'PE',
    tariff: 6,
  },
  {
    origin: 'AC',
    destination: 'PI',
    tariff: 5,
  },
  {
    origin: 'AC',
    destination: 'PR',
    tariff: 5,
  },
  {
    origin: 'AC',
    destination: 'RJ',
    tariff: 6,
  },
  {
    origin: 'AC',
    destination: 'RN',
    tariff: 6,
  },
  {
    origin: 'AC',
    destination: 'RO',
    tariff: 1,
  },
  {
    origin: 'AC',
    destination: 'RR',
    tariff: 4,
  },
  {
    origin: 'AC',
    destination: 'RS',
    tariff: 5,
  },
  {
    origin: 'AC',
    destination: 'SC',
    tariff: 5,
  },
  {
    origin: 'AC',
    destination: 'SE',
    tariff: 6,
  },
  {
    origin: 'AC',
    destination: 'SP',
    tariff: 5,
  },
  {
    origin: 'AC',
    destination: 'TO',
    tariff: 5,
  },
  {
    origin: 'AL',
    destination: 'AC',
    tariff: 6,
  },
  {
    origin: 'AL',
    destination: 'AL',
    tariff: 4,
  },
  {
    origin: 'AL',
    destination: 'AM',
    tariff: 6,
  },
  {
    origin: 'AL',
    destination: 'AP',
    tariff: 5,
  },
  {
    origin: 'AL',
    destination: 'BA',
    tariff: 1,
  },
  {
    origin: 'AL',
    destination: 'CE',
    tariff: 2,
  },
  {
    origin: 'AL',
    destination: 'DF',
    tariff: 4,
  },
  {
    origin: 'AL',
    destination: 'ES',
    tariff: 3,
  },
  {
    origin: 'AL',
    destination: 'MA',
    tariff: 3,
  },
  {
    origin: 'AL',
    destination: 'MG',
    tariff: 4,
  },
  {
    origin: 'AL',
    destination: 'MS',
    tariff: 4,
  },
  {
    origin: 'AL',
    destination: 'MT',
    tariff: 5,
  },
  {
    origin: 'AL',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'AL',
    destination: 'PB',
    tariff: 1,
  },
  {
    origin: 'AL',
    destination: 'PE',
    tariff: 1,
  },
  {
    origin: 'AL',
    destination: 'PI',
    tariff: 2,
  },
  {
    origin: 'AL',
    destination: 'PR',
    tariff: 5,
  },
  {
    origin: 'AL',
    destination: 'RJ',
    tariff: 4,
  },
  {
    origin: 'AL',
    destination: 'RN',
    tariff: 1,
  },
  {
    origin: 'AL',
    destination: 'RO',
    tariff: 6,
  },
  {
    origin: 'AL',
    destination: 'RR',
    tariff: 6,
  },
  {
    origin: 'AL',
    destination: 'RS',
    tariff: 5,
  },
  {
    origin: 'AL',
    destination: 'SC',
    tariff: 5,
  },
  {
    origin: 'AL',
    destination: 'SE',
    tariff: 1,
  },
  {
    origin: 'AL',
    destination: 'SP',
    tariff: 4,
  },
  {
    origin: 'AL',
    destination: 'TO',
    tariff: 5,
  },
  {
    origin: 'AL',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'AM',
    destination: 'AC',
    tariff: 3,
  },
  {
    origin: 'AM',
    destination: 'AL',
    tariff: 6,
  },
  {
    origin: 'AM',
    destination: 'AM',
    tariff: 4,
  },
  {
    origin: 'AM',
    destination: 'AP',
    tariff: 4,
  },
  {
    origin: 'AM',
    destination: 'BA',
    tariff: 5,
  },
  {
    origin: 'AM',
    destination: 'CE',
    tariff: 5,
  },
  {
    origin: 'AM',
    destination: 'DF',
    tariff: 4,
  },
  {
    origin: 'AM',
    destination: 'ES',
    tariff: 5,
  },
  {
    origin: 'AM',
    destination: 'GO',
    tariff: 5,
  },
  {
    origin: 'AM',
    destination: 'MA',
    tariff: 4,
  },
  {
    origin: 'AM',
    destination: 'MG',
    tariff: 5,
  },
  {
    origin: 'AM',
    destination: 'MS',
    tariff: 4,
  },
  {
    origin: 'AM',
    destination: 'MT',
    tariff: 4,
  },
  {
    origin: 'AM',
    destination: 'PA',
    tariff: 3,
  },
  {
    origin: 'AM',
    destination: 'PB',
    tariff: 5,
  },
  {
    origin: 'AM',
    destination: 'PE',
    tariff: 5,
  },
  {
    origin: 'AM',
    destination: 'PI',
    tariff: 4,
  },
  {
    origin: 'AM',
    destination: 'PR',
    tariff: 5,
  },
  {
    origin: 'AM',
    destination: 'RJ',
    tariff: 5,
  },
  {
    origin: 'AM',
    destination: 'RN',
    tariff: 5,
  },
  {
    origin: 'AM',
    destination: 'RO',
    tariff: 2,
  },
  {
    origin: 'AM',
    destination: 'RR',
    tariff: 2,
  },
  {
    origin: 'AM',
    destination: 'RS',
    tariff: 6,
  },
  {
    origin: 'AM',
    destination: 'SC',
    tariff: 6,
  },
  {
    origin: 'AM',
    destination: 'SE',
    tariff: 5,
  },
  {
    origin: 'AM',
    destination: 'SP',
    tariff: 5,
  },
  {
    origin: 'AM',
    destination: 'TO',
    tariff: 4,
  },
  {
    origin: 'AP',
    destination: 'AC',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'AL',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'AM',
    tariff: 4,
  },
  {
    origin: 'AP',
    destination: 'AP',
    tariff: 4,
  },
  {
    origin: 'AP',
    destination: 'BA',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'CE',
    tariff: 4,
  },
  {
    origin: 'AP',
    destination: 'DF',
    tariff: 4,
  },
  {
    origin: 'AP',
    destination: 'ES',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'GO',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'MA',
    tariff: 3,
  },
  {
    origin: 'AP',
    destination: 'MG',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'MS',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'MT',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'PA',
    tariff: 1,
  },
  {
    origin: 'AP',
    destination: 'PB',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'PE',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'PI',
    tariff: 3,
  },
  {
    origin: 'AP',
    destination: 'PR',
    tariff: 6,
  },
  {
    origin: 'AP',
    destination: 'RJ',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'RN',
    tariff: 4,
  },
  {
    origin: 'AP',
    destination: 'RO',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'RR',
    tariff: 4,
  },
  {
    origin: 'AP',
    destination: 'RS',
    tariff: 6,
  },
  {
    origin: 'AP',
    destination: 'SC',
    tariff: 6,
  },
  {
    origin: 'AP',
    destination: 'SE',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'SP',
    tariff: 5,
  },
  {
    origin: 'AP',
    destination: 'TO',
    tariff: 3,
  },
  {
    origin: 'BA',
    destination: 'AC',
    tariff: 6,
  },
  {
    origin: 'BA',
    destination: 'AL',
    tariff: 1,
  },
  {
    origin: 'BA',
    destination: 'AM',
    tariff: 5,
  },
  {
    origin: 'BA',
    destination: 'AP',
    tariff: 5,
  },
  {
    origin: 'BA',
    destination: 'BA',
    tariff: 4,
  },
  {
    origin: 'BA',
    destination: 'CE',
    tariff: 3,
  },
  {
    origin: 'BA',
    destination: 'DF',
    tariff: 3,
  },
  {
    origin: 'BA',
    destination: 'ES',
    tariff: 2,
  },
  {
    origin: 'BA',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'BA',
    destination: 'MA',
    tariff: 3,
  },
  {
    origin: 'BA',
    destination: 'MG',
    tariff: 3,
  },
  {
    origin: 'BA',
    destination: 'MS',
    tariff: 4,
  },
  {
    origin: 'BA',
    destination: 'MT',
    tariff: 4,
  },
  {
    origin: 'BA',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'BA',
    destination: 'PB',
    tariff: 2,
  },
  {
    origin: 'BA',
    destination: 'PE',
    tariff: 2,
  },
  {
    origin: 'BA',
    destination: 'PI',
    tariff: 3,
  },
  {
    origin: 'BA',
    destination: 'PR',
    tariff: 4,
  },
  {
    origin: 'BA',
    destination: 'RJ',
    tariff: 3,
  },
  {
    origin: 'BA',
    destination: 'RN',
    tariff: 2,
  },
  {
    origin: 'BA',
    destination: 'RO',
    tariff: 5,
  },
  {
    origin: 'BA',
    destination: 'RR',
    tariff: 6,
  },
  {
    origin: 'BA',
    destination: 'RS',
    tariff: 5,
  },
  {
    origin: 'BA',
    destination: 'SC',
    tariff: 4,
  },
  {
    origin: 'BA',
    destination: 'SE',
    tariff: 1,
  },
  {
    origin: 'BA',
    destination: 'SP',
    tariff: 4,
  },
  {
    origin: 'BA',
    destination: 'TO',
    tariff: 4,
  },
  {
    origin: 'CE',
    destination: 'AC',
    tariff: 6,
  },
  {
    origin: 'CE',
    destination: 'AL',
    tariff: 2,
  },
  {
    origin: 'CE',
    destination: 'AM',
    tariff: 5,
  },
  {
    origin: 'CE',
    destination: 'AP',
    tariff: 4,
  },
  {
    origin: 'CE',
    destination: 'BA',
    tariff: 3,
  },
  {
    origin: 'CE',
    destination: 'CE',
    tariff: 4,
  },
  {
    origin: 'CE',
    destination: 'DF',
    tariff: 4,
  },
  {
    origin: 'CE',
    destination: 'ES',
    tariff: 4,
  },
  {
    origin: 'CE',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'CE',
    destination: 'MA',
    tariff: 1,
  },
  {
    origin: 'CE',
    destination: 'MG',
    tariff: 4,
  },
  {
    origin: 'CE',
    destination: 'MS',
    tariff: 5,
  },
  {
    origin: 'CE',
    destination: 'MT',
    tariff: 5,
  },
  {
    origin: 'CE',
    destination: 'PA',
    tariff: 3,
  },
  {
    origin: 'CE',
    destination: 'PB',
    tariff: 1,
  },
  {
    origin: 'CE',
    destination: 'PE',
    tariff: 1,
  },
  {
    origin: 'CE',
    destination: 'PI',
    tariff: 1,
  },
  {
    origin: 'CE',
    destination: 'PR',
    tariff: 5,
  },
  {
    origin: 'CE',
    destination: 'RJ',
    tariff: 5,
  },
  {
    origin: 'CE',
    destination: 'RN',
    tariff: 1,
  },
  {
    origin: 'CE',
    destination: 'RO',
    tariff: 5,
  },
  {
    origin: 'CE',
    destination: 'RR',
    tariff: 5,
  },
  {
    origin: 'CE',
    destination: 'RS',
    tariff: 6,
  },
  {
    origin: 'CE',
    destination: 'SC',
    tariff: 6,
  },
  {
    origin: 'CE',
    destination: 'SE',
    tariff: 2,
  },
  {
    origin: 'CE',
    destination: 'SP',
    tariff: 5,
  },
  {
    origin: 'CE',
    destination: 'TO',
    tariff: 5,
  },
  {
    origin: 'DF',
    destination: 'AC',
    tariff: 5,
  },
  {
    origin: 'DF',
    destination: 'AL',
    tariff: 4,
  },
  {
    origin: 'DF',
    destination: 'AM',
    tariff: 4,
  },
  {
    origin: 'DF',
    destination: 'AP',
    tariff: 4,
  },
  {
    origin: 'DF',
    destination: 'BA',
    tariff: 3,
  },
  {
    origin: 'DF',
    destination: 'CE',
    tariff: 4,
  },
  {
    origin: 'DF',
    destination: 'DF',
    tariff: 3,
  },
  {
    origin: 'DF',
    destination: 'ES',
    tariff: 3,
  },
  {
    origin: 'DF',
    destination: 'GO',
    tariff: 1,
  },
  {
    origin: 'DF',
    destination: 'MA',
    tariff: 4,
  },
  {
    origin: 'DF',
    destination: 'MG',
    tariff: 1,
  },
  {
    origin: 'DF',
    destination: 'MS',
    tariff: 2,
  },
  {
    origin: 'DF',
    destination: 'MT',
    tariff: 2,
  },
  {
    origin: 'DF',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'DF',
    destination: 'PB',
    tariff: 4,
  },
  {
    origin: 'DF',
    destination: 'PE',
    tariff: 4,
  },
  {
    origin: 'DF',
    destination: 'PI',
    tariff: 3,
  },
  {
    origin: 'DF',
    destination: 'PR',
    tariff: 3,
  },
  {
    origin: 'DF',
    destination: 'RJ',
    tariff: 2,
  },
  {
    origin: 'DF',
    destination: 'RN',
    tariff: 4,
  },
  {
    origin: 'DF',
    destination: 'RO',
    tariff: 4,
  },
  {
    origin: 'DF',
    destination: 'RR',
    tariff: 5,
  },
  {
    origin: 'DF',
    destination: 'RS',
    tariff: 4,
  },
  {
    origin: 'DF',
    destination: 'SC',
    tariff: 3,
  },
  {
    origin: 'DF',
    destination: 'SE',
    tariff: 3,
  },
  {
    origin: 'DF',
    destination: 'SP',
    tariff: 2,
  },
  {
    origin: 'DF',
    destination: 'TO',
    tariff: 1,
  },
  {
    origin: 'ES',
    destination: 'AC',
    tariff: 6,
  },
  {
    origin: 'ES',
    destination: 'AL',
    tariff: 3,
  },
  {
    origin: 'ES',
    destination: 'AM',
    tariff: 5,
  },
  {
    origin: 'ES',
    destination: 'AP',
    tariff: 5,
  },
  {
    origin: 'ES',
    destination: 'BA',
    tariff: 2,
  },
  {
    origin: 'ES',
    destination: 'CE',
    tariff: 4,
  },
  {
    origin: 'ES',
    destination: 'DF',
    tariff: 3,
  },
  {
    origin: 'ES',
    destination: 'ES',
    tariff: 4,
  },
  {
    origin: 'ES',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'ES',
    destination: 'MA',
    tariff: 4,
  },
  {
    origin: 'ES',
    destination: 'MG',
    tariff: 1,
  },
  {
    origin: 'ES',
    destination: 'MS',
    tariff: 3,
  },
  {
    origin: 'ES',
    destination: 'MT',
    tariff: 4,
  },
  {
    origin: 'ES',
    destination: 'PA',
    tariff: 5,
  },
  {
    origin: 'ES',
    destination: 'PB',
    tariff: 4,
  },
  {
    origin: 'ES',
    destination: 'PE',
    tariff: 4,
  },
  {
    origin: 'ES',
    destination: 'PI',
    tariff: 4,
  },
  {
    origin: 'ES',
    destination: 'PR',
    tariff: 3,
  },
  {
    origin: 'ES',
    destination: 'RJ',
    tariff: 1,
  },
  {
    origin: 'ES',
    destination: 'RN',
    tariff: 4,
  },
  {
    origin: 'ES',
    destination: 'RO',
    tariff: 5,
  },
  {
    origin: 'ES',
    destination: 'RR',
    tariff: 6,
  },
  {
    origin: 'ES',
    destination: 'RS',
    tariff: 4,
  },
  {
    origin: 'ES',
    destination: 'SC',
    tariff: 3,
  },
  {
    origin: 'ES',
    destination: 'SE',
    tariff: 3,
  },
  {
    origin: 'ES',
    destination: 'SP',
    tariff: 2,
  },
  {
    origin: 'ES',
    destination: 'TO',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'AC',
    tariff: 5,
  },
  {
    origin: 'GO',
    destination: 'AL',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'AM',
    tariff: 5,
  },
  {
    origin: 'GO',
    destination: 'AP',
    tariff: 5,
  },
  {
    origin: 'GO',
    destination: 'BA',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'CE',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'DF',
    tariff: 1,
  },
  {
    origin: 'GO',
    destination: 'ES',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'MA',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'MG',
    tariff: 2,
  },
  {
    origin: 'GO',
    destination: 'MS',
    tariff: 2,
  },
  {
    origin: 'GO',
    destination: 'MT',
    tariff: 2,
  },
  {
    origin: 'GO',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'PB',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'PE',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'PI',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'PR',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'RJ',
    tariff: 3,
  },
  {
    origin: 'GO',
    destination: 'RN',
    tariff: 5,
  },
  {
    origin: 'GO',
    destination: 'RO',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'RR',
    tariff: 5,
  },
  {
    origin: 'GO',
    destination: 'RS',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'SC',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'SE',
    tariff: 4,
  },
  {
    origin: 'GO',
    destination: 'SP',
    tariff: 3,
  },
  {
    origin: 'GO',
    destination: 'TO',
    tariff: 1,
  },
  {
    origin: 'MA',
    destination: 'AC',
    tariff: 5,
  },
  {
    origin: 'MA',
    destination: 'AL',
    tariff: 3,
  },
  {
    origin: 'MA',
    destination: 'AM',
    tariff: 4,
  },
  {
    origin: 'MA',
    destination: 'AP',
    tariff: 3,
  },
  {
    origin: 'MA',
    destination: 'BA',
    tariff: 3,
  },
  {
    origin: 'MA',
    destination: 'CE',
    tariff: 1,
  },
  {
    origin: 'MA',
    destination: 'DF',
    tariff: 4,
  },
  {
    origin: 'MA',
    destination: 'ES',
    tariff: 4,
  },
  {
    origin: 'MA',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'MA',
    destination: 'MA',
    tariff: 4,
  },
  {
    origin: 'MA',
    destination: 'MG',
    tariff: 4,
  },
  {
    origin: 'MA',
    destination: 'MS',
    tariff: 5,
  },
  {
    origin: 'MA',
    destination: 'MT',
    tariff: 4,
  },
  {
    origin: 'MA',
    destination: 'PA',
    tariff: 1,
  },
  {
    origin: 'MA',
    destination: 'PB',
    tariff: 3,
  },
  {
    origin: 'MA',
    destination: 'PE',
    tariff: 3,
  },
  {
    origin: 'MA',
    destination: 'PI',
    tariff: 1,
  },
  {
    origin: 'MA',
    destination: 'PR',
    tariff: 5,
  },
  {
    origin: 'MA',
    destination: 'RJ',
    tariff: 5,
  },
  {
    origin: 'MA',
    destination: 'RN',
    tariff: 3,
  },
  {
    origin: 'MA',
    destination: 'RO',
    tariff: 5,
  },
  {
    origin: 'MA',
    destination: 'RR',
    tariff: 4,
  },
  {
    origin: 'MA',
    destination: 'RS',
    tariff: 6,
  },
  {
    origin: 'MA',
    destination: 'SC',
    tariff: 5,
  },
  {
    origin: 'MA',
    destination: 'SE',
    tariff: 3,
  },
  {
    origin: 'MA',
    destination: 'SP',
    tariff: 5,
  },
  {
    origin: 'MA',
    destination: 'TO',
    tariff: 5,
  },
  {
    origin: 'MG',
    destination: 'AC',
    tariff: 6,
  },
  {
    origin: 'MG',
    destination: 'AL',
    tariff: 4,
  },
  {
    origin: 'MG',
    destination: 'AM',
    tariff: 5,
  },
  {
    origin: 'MG',
    destination: 'AP',
    tariff: 6,
  },
  {
    origin: 'MG',
    destination: 'BA',
    tariff: 2,
  },
  {
    origin: 'MG',
    destination: 'CE',
    tariff: 4,
  },
  {
    origin: 'MG',
    destination: 'DF',
    tariff: 1,
  },
  {
    origin: 'MG',
    destination: 'ES',
    tariff: 2,
  },
  {
    origin: 'MG',
    destination: 'GO',
    tariff: 2,
  },
  {
    origin: 'MG',
    destination: 'MA',
    tariff: 4,
  },
  {
    origin: 'MG',
    destination: 'MG',
    tariff: 3,
  },
  {
    origin: 'MG',
    destination: 'MS',
    tariff: 2,
  },
  {
    origin: 'MG',
    destination: 'MT',
    tariff: 3,
  },
  {
    origin: 'MG',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'MG',
    destination: 'PB',
    tariff: 4,
  },
  {
    origin: 'MG',
    destination: 'PE',
    tariff: 4,
  },
  {
    origin: 'MG',
    destination: 'PI',
    tariff: 5,
  },
  {
    origin: 'MG',
    destination: 'PR',
    tariff: 2,
  },
  {
    origin: 'MG',
    destination: 'RJ',
    tariff: 1,
  },
  {
    origin: 'MG',
    destination: 'RN',
    tariff: 5,
  },
  {
    origin: 'MG',
    destination: 'RO',
    tariff: 5,
  },
  {
    origin: 'MG',
    destination: 'RR',
    tariff: 6,
  },
  {
    origin: 'MG',
    destination: 'RS',
    tariff: 3,
  },
  {
    origin: 'MG',
    destination: 'SC',
    tariff: 2,
  },
  {
    origin: 'MG',
    destination: 'SE',
    tariff: 4,
  },
  {
    origin: 'MG',
    destination: 'SP',
    tariff: 1,
  },
  {
    origin: 'MG',
    destination: 'TO',
    tariff: 4,
  },
  {
    origin: 'MS',
    destination: 'AC',
    tariff: 3,
  },
  {
    origin: 'MS',
    destination: 'AL',
    tariff: 4,
  },
  {
    origin: 'MS',
    destination: 'AM',
    tariff: 4,
  },
  {
    origin: 'MS',
    destination: 'AP',
    tariff: 5,
  },
  {
    origin: 'MS',
    destination: 'BA',
    tariff: 4,
  },
  {
    origin: 'MS',
    destination: 'CE',
    tariff: 5,
  },
  {
    origin: 'MS',
    destination: 'DF',
    tariff: 2,
  },
  {
    origin: 'MS',
    destination: 'ES',
    tariff: 3,
  },
  {
    origin: 'MS',
    destination: 'GO',
    tariff: 2,
  },
  {
    origin: 'MS',
    destination: 'MA',
    tariff: 5,
  },
  {
    origin: 'MS',
    destination: 'MG',
    tariff: 2,
  },
  {
    origin: 'MS',
    destination: 'MS',
    tariff: 4,
  },
  {
    origin: 'MS',
    destination: 'MT',
    tariff: 1,
  },
  {
    origin: 'MS',
    destination: 'PA',
    tariff: 5,
  },
  {
    origin: 'MS',
    destination: 'PB',
    tariff: 5,
  },
  {
    origin: 'MS',
    destination: 'PE',
    tariff: 5,
  },
  {
    origin: 'MS',
    destination: 'PI',
    tariff: 5,
  },
  {
    origin: 'MS',
    destination: 'PR',
    tariff: 2,
  },
  {
    origin: 'MS',
    destination: 'RJ',
    tariff: 2,
  },
  {
    origin: 'MS',
    destination: 'RN',
    tariff: 5,
  },
  {
    origin: 'MS',
    destination: 'RO',
    tariff: 3,
  },
  {
    origin: 'MS',
    destination: 'RR',
    tariff: 5,
  },
  {
    origin: 'MS',
    destination: 'RS',
    tariff: 3,
  },
  {
    origin: 'MS',
    destination: 'SC',
    tariff: 2,
  },
  {
    origin: 'MS',
    destination: 'SE',
    tariff: 4,
  },
  {
    origin: 'MS',
    destination: 'SP',
    tariff: 2,
  },
  {
    origin: 'MS',
    destination: 'TO',
    tariff: 3,
  },
  {
    origin: 'MT',
    destination: 'AC',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'AL',
    tariff: 5,
  },
  {
    origin: 'MT',
    destination: 'AM',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'AP',
    tariff: 5,
  },
  {
    origin: 'MT',
    destination: 'BA',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'CE',
    tariff: 5,
  },
  {
    origin: 'MT',
    destination: 'DF',
    tariff: 2,
  },
  {
    origin: 'MT',
    destination: 'ES',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'GO',
    tariff: 2,
  },
  {
    origin: 'MT',
    destination: 'MA',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'MG',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'MS',
    tariff: 1,
  },
  {
    origin: 'MT',
    destination: 'MT',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'PB',
    tariff: 5,
  },
  {
    origin: 'MT',
    destination: 'PE',
    tariff: 5,
  },
  {
    origin: 'MT',
    destination: 'PI',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'PR',
    tariff: 3,
  },
  {
    origin: 'MT',
    destination: 'RJ',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'RN',
    tariff: 5,
  },
  {
    origin: 'MT',
    destination: 'RO',
    tariff: 3,
  },
  {
    origin: 'MT',
    destination: 'RR',
    tariff: 5,
  },
  {
    origin: 'MT',
    destination: 'RS',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'SC',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'SE',
    tariff: 5,
  },
  {
    origin: 'MT',
    destination: 'SP',
    tariff: 4,
  },
  {
    origin: 'MT',
    destination: 'TO',
    tariff: 3,
  },
  {
    origin: 'PA',
    destination: 'AC',
    tariff: 5,
  },
  {
    origin: 'PA',
    destination: 'AL',
    tariff: 4,
  },
  {
    origin: 'PA',
    destination: 'AM',
    tariff: 3,
  },
  {
    origin: 'PA',
    destination: 'AP',
    tariff: 1,
  },
  {
    origin: 'PA',
    destination: 'BA',
    tariff: 4,
  },
  {
    origin: 'PA',
    destination: 'CE',
    tariff: 3,
  },
  {
    origin: 'PA',
    destination: 'DF',
    tariff: 4,
  },
  {
    origin: 'PA',
    destination: 'ES',
    tariff: 5,
  },
  {
    origin: 'PA',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'PA',
    destination: 'MA',
    tariff: 1,
  },
  {
    origin: 'PA',
    destination: 'MG',
    tariff: 5,
  },
  {
    origin: 'PA',
    destination: 'MS',
    tariff: 5,
  },
  {
    origin: 'PA',
    destination: 'MT',
    tariff: 4,
  },
  {
    origin: 'PA',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'PA',
    destination: 'PB',
    tariff: 4,
  },
  {
    origin: 'PA',
    destination: 'PE',
    tariff: 4,
  },
  {
    origin: 'PA',
    destination: 'PI',
    tariff: 2,
  },
  {
    origin: 'PA',
    destination: 'PR',
    tariff: 5,
  },
  {
    origin: 'PA',
    destination: 'RJ',
    tariff: 5,
  },
  {
    origin: 'PA',
    destination: 'RN',
    tariff: 4,
  },
  {
    origin: 'PA',
    destination: 'RO',
    tariff: 4,
  },
  {
    origin: 'PA',
    destination: 'RR',
    tariff: 4,
  },
  {
    origin: 'PA',
    destination: 'RS',
    tariff: 6,
  },
  {
    origin: 'PA',
    destination: 'SC',
    tariff: 6,
  },
  {
    origin: 'PA',
    destination: 'SE',
    tariff: 4,
  },
  {
    origin: 'PA',
    destination: 'SP',
    tariff: 5,
  },
  {
    origin: 'PA',
    destination: 'TO',
    tariff: 2,
  },
  {
    origin: 'PB',
    destination: 'AC',
    tariff: 6,
  },
  {
    origin: 'PB',
    destination: 'AL',
    tariff: 1,
  },
  {
    origin: 'PB',
    destination: 'AM',
    tariff: 5,
  },
  {
    origin: 'PB',
    destination: 'AP',
    tariff: 5,
  },
  {
    origin: 'PB',
    destination: 'BA',
    tariff: 2,
  },
  {
    origin: 'PB',
    destination: 'CE',
    tariff: 1,
  },
  {
    origin: 'PB',
    destination: 'DF',
    tariff: 4,
  },
  {
    origin: 'PB',
    destination: 'ES',
    tariff: 4,
  },
  {
    origin: 'PB',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'PB',
    destination: 'MA',
    tariff: 3,
  },
  {
    origin: 'PB',
    destination: 'MG',
    tariff: 4,
  },
  {
    origin: 'PB',
    destination: 'MS',
    tariff: 5,
  },
  {
    origin: 'PB',
    destination: 'MT',
    tariff: 5,
  },
  {
    origin: 'PB',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'PB',
    destination: 'PB',
    tariff: 4,
  },
  {
    origin: 'PB',
    destination: 'PE',
    tariff: 1,
  },
  {
    origin: 'PB',
    destination: 'PI',
    tariff: 2,
  },
  {
    origin: 'PB',
    destination: 'PR',
    tariff: 5,
  },
  {
    origin: 'PB',
    destination: 'RJ',
    tariff: 4,
  },
  {
    origin: 'PB',
    destination: 'RN',
    tariff: 1,
  },
  {
    origin: 'PB',
    destination: 'RO',
    tariff: 6,
  },
  {
    origin: 'PB',
    destination: 'RR',
    tariff: 6,
  },
  {
    origin: 'PB',
    destination: 'RS',
    tariff: 6,
  },
  {
    origin: 'PB',
    destination: 'SC',
    tariff: 5,
  },
  {
    origin: 'PB',
    destination: 'SE',
    tariff: 1,
  },
  {
    origin: 'PB',
    destination: 'SP',
    tariff: 5,
  },
  {
    origin: 'PB',
    destination: 'TO',
    tariff: 5,
  },
  {
    origin: 'PE',
    destination: 'AC',
    tariff: 6,
  },
  {
    origin: 'PE',
    destination: 'AL',
    tariff: 1,
  },
  {
    origin: 'PE',
    destination: 'AM',
    tariff: 5,
  },
  {
    origin: 'PE',
    destination: 'AP',
    tariff: 5,
  },
  {
    origin: 'PE',
    destination: 'BA',
    tariff: 2,
  },
  {
    origin: 'PE',
    destination: 'CE',
    tariff: 1,
  },
  {
    origin: 'PE',
    destination: 'DF',
    tariff: 4,
  },
  {
    origin: 'PE',
    destination: 'ES',
    tariff: 4,
  },
  {
    origin: 'PE',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'PE',
    destination: 'MA',
    tariff: 3,
  },
  {
    origin: 'PE',
    destination: 'MG',
    tariff: 4,
  },
  {
    origin: 'PE',
    destination: 'MS',
    tariff: 5,
  },
  {
    origin: 'PE',
    destination: 'MT',
    tariff: 5,
  },
  {
    origin: 'PE',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'PE',
    destination: 'PB',
    tariff: 1,
  },
  {
    origin: 'PE',
    destination: 'PE',
    tariff: 4,
  },
  {
    origin: 'PE',
    destination: 'PI',
    tariff: 2,
  },
  {
    origin: 'PE',
    destination: 'PR',
    tariff: 5,
  },
  {
    origin: 'PE',
    destination: 'RJ',
    tariff: 4,
  },
  {
    origin: 'PE',
    destination: 'RN',
    tariff: 1,
  },
  {
    origin: 'PE',
    destination: 'RO',
    tariff: 6,
  },
  {
    origin: 'PE',
    destination: 'RR',
    tariff: 6,
  },
  {
    origin: 'PE',
    destination: 'RS',
    tariff: 6,
  },
  {
    origin: 'PE',
    destination: 'SC',
    tariff: 5,
  },
  {
    origin: 'PE',
    destination: 'SE',
    tariff: 1,
  },
  {
    origin: 'PE',
    destination: 'SP',
    tariff: 5,
  },
  {
    origin: 'PE',
    destination: 'TO',
    tariff: 4,
  },
  {
    origin: 'PI',
    destination: 'AC',
    tariff: 5,
  },
  {
    origin: 'PI',
    destination: 'AL',
    tariff: 2,
  },
  {
    origin: 'PI',
    destination: 'AM',
    tariff: 4,
  },
  {
    origin: 'PI',
    destination: 'AP',
    tariff: 3,
  },
  {
    origin: 'PI',
    destination: 'BA',
    tariff: 3,
  },
  {
    origin: 'PI',
    destination: 'CE',
    tariff: 1,
  },
  {
    origin: 'PI',
    destination: 'DF',
    tariff: 3,
  },
  {
    origin: 'PI',
    destination: 'ES',
    tariff: 4,
  },
  {
    origin: 'PI',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'PI',
    destination: 'MA',
    tariff: 1,
  },
  {
    origin: 'PI',
    destination: 'MG',
    tariff: 4,
  },
  {
    origin: 'PI',
    destination: 'MS',
    tariff: 5,
  },
  {
    origin: 'PI',
    destination: 'MT',
    tariff: 4,
  },
  {
    origin: 'PI',
    destination: 'PA',
    tariff: 2,
  },
  {
    origin: 'PI',
    destination: 'PB',
    tariff: 2,
  },
  {
    origin: 'PI',
    destination: 'PE',
    tariff: 2,
  },
  {
    origin: 'PI',
    destination: 'PI',
    tariff: 4,
  },
  {
    origin: 'PI',
    destination: 'PR',
    tariff: 5,
  },
  {
    origin: 'PI',
    destination: 'RJ',
    tariff: 4,
  },
  {
    origin: 'PI',
    destination: 'RN',
    tariff: 2,
  },
  {
    origin: 'PI',
    destination: 'RO',
    tariff: 5,
  },
  {
    origin: 'PI',
    destination: 'RR',
    tariff: 5,
  },
  {
    origin: 'PI',
    destination: 'RS',
    tariff: 6,
  },
  {
    origin: 'PI',
    destination: 'SC',
    tariff: 5,
  },
  {
    origin: 'PI',
    destination: 'SE',
    tariff: 4,
  },
  {
    origin: 'PI',
    destination: 'SP',
    tariff: 5,
  },
  {
    origin: 'PI',
    destination: 'TO',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'AC',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'AL',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'AM',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'AP',
    tariff: 6,
  },
  {
    origin: 'PR',
    destination: 'BA',
    tariff: 4,
  },
  {
    origin: 'PR',
    destination: 'CE',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'DF',
    tariff: 3,
  },
  {
    origin: 'PR',
    destination: 'ES',
    tariff: 3,
  },
  {
    origin: 'PR',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'PR',
    destination: 'MA',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'MG',
    tariff: 2,
  },
  {
    origin: 'PR',
    destination: 'MS',
    tariff: 2,
  },
  {
    origin: 'PR',
    destination: 'MT',
    tariff: 3,
  },
  {
    origin: 'PR',
    destination: 'PA',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'PB',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'PE',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'PI',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'PR',
    tariff: 3,
  },
  {
    origin: 'PR',
    destination: 'RJ',
    tariff: 2,
  },
  {
    origin: 'PR',
    destination: 'RN',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'RO',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'RR',
    tariff: 6,
  },
  {
    origin: 'PR',
    destination: 'RS',
    tariff: 1,
  },
  {
    origin: 'PR',
    destination: 'SC',
    tariff: 1,
  },
  {
    origin: 'PR',
    destination: 'SE',
    tariff: 5,
  },
  {
    origin: 'PR',
    destination: 'SP',
    tariff: 1,
  },
  {
    origin: 'PR',
    destination: 'TO',
    tariff: 4,
  },
  {
    origin: 'RJ',
    destination: 'AC',
    tariff: 6,
  },
  {
    origin: 'RJ',
    destination: 'AL',
    tariff: 4,
  },
  {
    origin: 'RJ',
    destination: 'AM',
    tariff: 5,
  },
  {
    origin: 'RJ',
    destination: 'AP',
    tariff: 5,
  },
  {
    origin: 'RJ',
    destination: 'BA',
    tariff: 3,
  },
  {
    origin: 'RJ',
    destination: 'CE',
    tariff: 4,
  },
  {
    origin: 'RJ',
    destination: 'DF',
    tariff: 1,
  },
  {
    origin: 'RJ',
    destination: 'ES',
    tariff: 1,
  },
  {
    origin: 'RJ',
    destination: 'GO',
    tariff: 2,
  },
  {
    origin: 'RJ',
    destination: 'MA',
    tariff: 4,
  },
  {
    origin: 'RJ',
    destination: 'MG',
    tariff: 1,
  },
  {
    origin: 'RJ',
    destination: 'MS',
    tariff: 2,
  },
  {
    origin: 'RJ',
    destination: 'MT',
    tariff: 3,
  },
  {
    origin: 'RJ',
    destination: 'PA',
    tariff: 5,
  },
  {
    origin: 'RJ',
    destination: 'PB',
    tariff: 4,
  },
  {
    origin: 'RJ',
    destination: 'PE',
    tariff: 4,
  },
  {
    origin: 'RJ',
    destination: 'PI',
    tariff: 4,
  },
  {
    origin: 'RJ',
    destination: 'PR',
    tariff: 1,
  },
  {
    origin: 'RJ',
    destination: 'RJ',
    tariff: 2,
  },
  {
    origin: 'RJ',
    destination: 'RN',
    tariff: 4,
  },
  {
    origin: 'RJ',
    destination: 'RO',
    tariff: 4,
  },
  {
    origin: 'RJ',
    destination: 'RR',
    tariff: 6,
  },
  {
    origin: 'RJ',
    destination: 'RS',
    tariff: 2,
  },
  {
    origin: 'RJ',
    destination: 'SC',
    tariff: 2,
  },
  {
    origin: 'RJ',
    destination: 'SE',
    tariff: 3,
  },
  {
    origin: 'RJ',
    destination: 'SP',
    tariff: 1,
  },
  {
    origin: 'RJ',
    destination: 'TO',
    tariff: 3,
  },
  {
    origin: 'RN',
    destination: 'AC',
    tariff: 6,
  },
  {
    origin: 'RN',
    destination: 'AL',
    tariff: 1,
  },
  {
    origin: 'RN',
    destination: 'AM',
    tariff: 5,
  },
  {
    origin: 'RN',
    destination: 'AP',
    tariff: 4,
  },
  {
    origin: 'RN',
    destination: 'BA',
    tariff: 2,
  },
  {
    origin: 'RN',
    destination: 'CE',
    tariff: 1,
  },
  {
    origin: 'RN',
    destination: 'DF',
    tariff: 4,
  },
  {
    origin: 'RN',
    destination: 'ES',
    tariff: 4,
  },
  {
    origin: 'RN',
    destination: 'GO',
    tariff: 5,
  },
  {
    origin: 'RN',
    destination: 'MA',
    tariff: 3,
  },
  {
    origin: 'RN',
    destination: 'MG',
    tariff: 4,
  },
  {
    origin: 'RN',
    destination: 'MS',
    tariff: 5,
  },
  {
    origin: 'RN',
    destination: 'MT',
    tariff: 5,
  },
  {
    origin: 'RN',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'RN',
    destination: 'PB',
    tariff: 1,
  },
  {
    origin: 'RN',
    destination: 'PE',
    tariff: 1,
  },
  {
    origin: 'RN',
    destination: 'PI',
    tariff: 2,
  },
  {
    origin: 'RN',
    destination: 'PR',
    tariff: 5,
  },
  {
    origin: 'RN',
    destination: 'RJ',
    tariff: 5,
  },
  {
    origin: 'RN',
    destination: 'RN',
    tariff: 4,
  },
  {
    origin: 'RN',
    destination: 'RO',
    tariff: 6,
  },
  {
    origin: 'RN',
    destination: 'RR',
    tariff: 6,
  },
  {
    origin: 'RN',
    destination: 'RS',
    tariff: 6,
  },
  {
    origin: 'RN',
    destination: 'SC',
    tariff: 5,
  },
  {
    origin: 'RN',
    destination: 'SE',
    tariff: 2,
  },
  {
    origin: 'RN',
    destination: 'SP',
    tariff: 5,
  },
  {
    origin: 'RN',
    destination: 'TO',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'AC',
    tariff: 1,
  },
  {
    origin: 'RO',
    destination: 'AL',
    tariff: 6,
  },
  {
    origin: 'RO',
    destination: 'AM',
    tariff: 2,
  },
  {
    origin: 'RO',
    destination: 'AP',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'BA',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'CE',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'DF',
    tariff: 4,
  },
  {
    origin: 'RO',
    destination: 'ES',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'RO',
    destination: 'MA',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'MG',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'MS',
    tariff: 3,
  },
  {
    origin: 'RO',
    destination: 'MT',
    tariff: 3,
  },
  {
    origin: 'RO',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'RO',
    destination: 'PB',
    tariff: 6,
  },
  {
    origin: 'RO',
    destination: 'PE',
    tariff: 6,
  },
  {
    origin: 'RO',
    destination: 'PI',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'PR',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'RJ',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'RN',
    tariff: 6,
  },
  {
    origin: 'RO',
    destination: 'RO',
    tariff: 4,
  },
  {
    origin: 'RO',
    destination: 'RR',
    tariff: 3,
  },
  {
    origin: 'RO',
    destination: 'RS',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'SC',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'SE',
    tariff: 6,
  },
  {
    origin: 'RO',
    destination: 'SP',
    tariff: 5,
  },
  {
    origin: 'RO',
    destination: 'TO',
    tariff: 5,
  },
  {
    origin: 'RR',
    destination: 'AC',
    tariff: 4,
  },
  {
    origin: 'RR',
    destination: 'AL',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'AM',
    tariff: 2,
  },
  {
    origin: 'RR',
    destination: 'AP',
    tariff: 4,
  },
  {
    origin: 'RR',
    destination: 'BA',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'CE',
    tariff: 5,
  },
  {
    origin: 'RR',
    destination: 'DF',
    tariff: 5,
  },
  {
    origin: 'RR',
    destination: 'ES',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'GO',
    tariff: 5,
  },
  {
    origin: 'RR',
    destination: 'MA',
    tariff: 4,
  },
  {
    origin: 'RR',
    destination: 'MG',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'MS',
    tariff: 5,
  },
  {
    origin: 'RR',
    destination: 'MT',
    tariff: 5,
  },
  {
    origin: 'RR',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'RR',
    destination: 'PB',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'PE',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'PI',
    tariff: 5,
  },
  {
    origin: 'RR',
    destination: 'PR',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'RJ',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'RN',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'RO',
    tariff: 3,
  },
  {
    origin: 'RR',
    destination: 'RR',
    tariff: 4,
  },
  {
    origin: 'RR',
    destination: 'RS',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'SC',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'SE',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'SP',
    tariff: 6,
  },
  {
    origin: 'RR',
    destination: 'TO',
    tariff: 4,
  },
  {
    origin: 'RS',
    destination: 'AC',
    tariff: 5,
  },
  {
    origin: 'RS',
    destination: 'AL',
    tariff: 5,
  },
  {
    origin: 'RS',
    destination: 'AM',
    tariff: 6,
  },
  {
    origin: 'RS',
    destination: 'AP',
    tariff: 6,
  },
  {
    origin: 'RS',
    destination: 'BA',
    tariff: 5,
  },
  {
    origin: 'RS',
    destination: 'CE',
    tariff: 6,
  },
  {
    origin: 'RS',
    destination: 'DF',
    tariff: 4,
  },
  {
    origin: 'RS',
    destination: 'ES',
    tariff: 4,
  },
  {
    origin: 'RS',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'RS',
    destination: 'MA',
    tariff: 6,
  },
  {
    origin: 'RS',
    destination: 'MG',
    tariff: 4,
  },
  {
    origin: 'RS',
    destination: 'MS',
    tariff: 3,
  },
  {
    origin: 'RS',
    destination: 'MT',
    tariff: 4,
  },
  {
    origin: 'RS',
    destination: 'PA',
    tariff: 6,
  },
  {
    origin: 'RS',
    destination: 'PB',
    tariff: 6,
  },
  {
    origin: 'RS',
    destination: 'PE',
    tariff: 6,
  },
  {
    origin: 'RS',
    destination: 'PI',
    tariff: 6,
  },
  {
    origin: 'RS',
    destination: 'PR',
    tariff: 1,
  },
  {
    origin: 'RS',
    destination: 'RJ',
    tariff: 3,
  },
  {
    origin: 'RS',
    destination: 'RN',
    tariff: 6,
  },
  {
    origin: 'RS',
    destination: 'RO',
    tariff: 5,
  },
  {
    origin: 'RS',
    destination: 'RR',
    tariff: 6,
  },
  {
    origin: 'RS',
    destination: 'RS',
    tariff: 3,
  },
  {
    origin: 'RS',
    destination: 'SC',
    tariff: 1,
  },
  {
    origin: 'RS',
    destination: 'SE',
    tariff: 5,
  },
  {
    origin: 'RS',
    destination: 'SP',
    tariff: 2,
  },
  {
    origin: 'RS',
    destination: 'TO',
    tariff: 5,
  },
  {
    origin: 'SC',
    destination: 'AC',
    tariff: 5,
  },
  {
    origin: 'SC',
    destination: 'AL',
    tariff: 5,
  },
  {
    origin: 'SC',
    destination: 'AM',
    tariff: 6,
  },
  {
    origin: 'SC',
    destination: 'AP',
    tariff: 6,
  },
  {
    origin: 'SC',
    destination: 'BA',
    tariff: 4,
  },
  {
    origin: 'SC',
    destination: 'CE',
    tariff: 6,
  },
  {
    origin: 'SC',
    destination: 'DF',
    tariff: 3,
  },
  {
    origin: 'SC',
    destination: 'ES',
    tariff: 3,
  },
  {
    origin: 'SC',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'SC',
    destination: 'MA',
    tariff: 5,
  },
  {
    origin: 'SC',
    destination: 'MG',
    tariff: 3,
  },
  {
    origin: 'SC',
    destination: 'MS',
    tariff: 2,
  },
  {
    origin: 'SC',
    destination: 'MT',
    tariff: 4,
  },
  {
    origin: 'SC',
    destination: 'PA',
    tariff: 6,
  },
  {
    origin: 'SC',
    destination: 'PB',
    tariff: 5,
  },
  {
    origin: 'SC',
    destination: 'PE',
    tariff: 5,
  },
  {
    origin: 'SC',
    destination: 'PI',
    tariff: 5,
  },
  {
    origin: 'SC',
    destination: 'PR',
    tariff: 1,
  },
  {
    origin: 'SC',
    destination: 'RJ',
    tariff: 2,
  },
  {
    origin: 'SC',
    destination: 'RN',
    tariff: 5,
  },
  {
    origin: 'SC',
    destination: 'RO',
    tariff: 5,
  },
  {
    origin: 'SC',
    destination: 'RR',
    tariff: 6,
  },
  {
    origin: 'SC',
    destination: 'RS',
    tariff: 1,
  },
  {
    origin: 'SC',
    destination: 'SC',
    tariff: 4,
  },
  {
    origin: 'SC',
    destination: 'SE',
    tariff: 5,
  },
  {
    origin: 'SC',
    destination: 'SP',
    tariff: 1,
  },
  {
    origin: 'SC',
    destination: 'TO',
    tariff: 4,
  },
  {
    origin: 'SE',
    destination: 'AC',
    tariff: 6,
  },
  {
    origin: 'SE',
    destination: 'AL',
    tariff: 1,
  },
  {
    origin: 'SE',
    destination: 'AM',
    tariff: 5,
  },
  {
    origin: 'SE',
    destination: 'AP',
    tariff: 5,
  },
  {
    origin: 'SE',
    destination: 'BA',
    tariff: 1,
  },
  {
    origin: 'SE',
    destination: 'CE',
    tariff: 2,
  },
  {
    origin: 'SE',
    destination: 'DF',
    tariff: 3,
  },
  {
    origin: 'SE',
    destination: 'ES',
    tariff: 3,
  },
  {
    origin: 'SE',
    destination: 'GO',
    tariff: 4,
  },
  {
    origin: 'SE',
    destination: 'MA',
    tariff: 3,
  },
  {
    origin: 'SE',
    destination: 'MG',
    tariff: 3,
  },
  {
    origin: 'SE',
    destination: 'MS',
    tariff: 4,
  },
  {
    origin: 'SE',
    destination: 'MT',
    tariff: 5,
  },
  {
    origin: 'SE',
    destination: 'PA',
    tariff: 4,
  },
  {
    origin: 'SE',
    destination: 'PB',
    tariff: 1,
  },
  {
    origin: 'SE',
    destination: 'PE',
    tariff: 1,
  },
  {
    origin: 'SE',
    destination: 'PI',
    tariff: 4,
  },
  {
    origin: 'SE',
    destination: 'PR',
    tariff: 5,
  },
  {
    origin: 'SE',
    destination: 'RJ',
    tariff: 4,
  },
  {
    origin: 'SE',
    destination: 'RN',
    tariff: 2,
  },
  {
    origin: 'SE',
    destination: 'RO',
    tariff: 6,
  },
  {
    origin: 'SE',
    destination: 'RR',
    tariff: 6,
  },
  {
    origin: 'SE',
    destination: 'RS',
    tariff: 5,
  },
  {
    origin: 'SE',
    destination: 'SC',
    tariff: 5,
  },
  {
    origin: 'SE',
    destination: 'SE',
    tariff: 4,
  },
  {
    origin: 'SE',
    destination: 'SP',
    tariff: 4,
  },
  {
    origin: 'SE',
    destination: 'TO',
    tariff: 5,
  },
  {
    origin: 'SP',
    destination: 'AC',
    tariff: 5,
  },
  {
    origin: 'SP',
    destination: 'AL',
    tariff: 4,
  },
  {
    origin: 'SP',
    destination: 'AM',
    tariff: 4,
  },
  {
    origin: 'SP',
    destination: 'AP',
    tariff: 4,
  },
  {
    origin: 'SP',
    destination: 'BA',
    tariff: 2,
  },
  {
    origin: 'SP',
    destination: 'CE',
    tariff: 3,
  },
  {
    origin: 'SP',
    destination: 'DF',
    tariff: 1,
  },
  {
    origin: 'SP',
    destination: 'ES',
    tariff: 1,
  },
  {
    origin: 'SP',
    destination: 'GO',
    tariff: 1,
  },
  {
    origin: 'SP',
    destination: 'MA',
    tariff: 4,
  },
  {
    origin: 'SP',
    destination: 'MG',
    tariff: 1,
  },
  {
    origin: 'SP',
    destination: 'MS',
    tariff: 1,
  },
  {
    origin: 'SP',
    destination: 'MT',
    tariff: 2,
  },
  {
    origin: 'SP',
    destination: 'PA',
    tariff: 3,
  },
  {
    origin: 'SP',
    destination: 'PB',
    tariff: 4,
  },
  {
    origin: 'SP',
    destination: 'PE',
    tariff: 3,
  },
  {
    origin: 'SP',
    destination: 'PI',
    tariff: 4,
  },
  {
    origin: 'SP',
    destination: 'PR',
    tariff: 1,
  },
  {
    origin: 'SP',
    destination: 'RJ',
    tariff: 1,
  },
  {
    origin: 'SP',
    destination: 'RN',
    tariff: 4,
  },
  {
    origin: 'SP',
    destination: 'RO',
    tariff: 4,
  },
  {
    origin: 'SP',
    destination: 'RR',
    tariff: 5,
  },
  {
    origin: 'SP',
    destination: 'RS',
    tariff: 1,
  },
  {
    origin: 'SP',
    destination: 'SC',
    tariff: 1,
  },
  {
    origin: 'SP',
    destination: 'SE',
    tariff: 4,
  },
  {
    origin: 'SP',
    destination: 'SP',
    tariff: 1,
  },
  {
    origin: 'SP',
    destination: 'TO',
    tariff: 3,
  },
  {
    origin: 'TO',
    destination: 'AC',
    tariff: 5,
  },
  {
    origin: 'TO',
    destination: 'AL',
    tariff: 5,
  },
  {
    origin: 'TO',
    destination: 'AM',
    tariff: 4,
  },
  {
    origin: 'TO',
    destination: 'AP',
    tariff: 3,
  },
  {
    origin: 'TO',
    destination: 'BA',
    tariff: 4,
  },
  {
    origin: 'TO',
    destination: 'CE',
    tariff: 5,
  },
  {
    origin: 'TO',
    destination: 'DF',
    tariff: 1,
  },
  {
    origin: 'TO',
    destination: 'ES',
    tariff: 4,
  },
  {
    origin: 'TO',
    destination: 'GO',
    tariff: 1,
  },
  {
    origin: 'TO',
    destination: 'MA',
    tariff: 5,
  },
  {
    origin: 'TO',
    destination: 'MG',
    tariff: 3,
  },
  {
    origin: 'TO',
    destination: 'MS',
    tariff: 3,
  },
  {
    origin: 'TO',
    destination: 'MT',
    tariff: 3,
  },
  {
    origin: 'TO',
    destination: 'PA',
    tariff: 2,
  },
  {
    origin: 'TO',
    destination: 'PB',
    tariff: 5,
  },
  {
    origin: 'TO',
    destination: 'PE',
    tariff: 4,
  },
  {
    origin: 'TO',
    destination: 'PI',
    tariff: 5,
  },
  {
    origin: 'TO',
    destination: 'PR',
    tariff: 4,
  },
  {
    origin: 'TO',
    destination: 'RJ',
    tariff: 3,
  },
  {
    origin: 'TO',
    destination: 'RN',
    tariff: 5,
  },
  {
    origin: 'TO',
    destination: 'RO',
    tariff: 5,
  },
  {
    origin: 'TO',
    destination: 'RR',
    tariff: 4,
  },
  {
    origin: 'TO',
    destination: 'RS',
    tariff: 5,
  },
  {
    origin: 'TO',
    destination: 'SC',
    tariff: 4,
  },
  {
    origin: 'TO',
    destination: 'SE',
    tariff: 5,
  },
  {
    origin: 'TO',
    destination: 'SP',
    tariff: 3,
  },
  {
    origin: 'TO',
    destination: 'TO',
    tariff: 4,
  },
  ], {}),

  down: (queryInterface) => {
    queryInterface.bulkDelete('OriginDestinationMatrices', [{
      origin: 'AC',
      destination: 'AC',
      tariff: 4,
    },
    {
      origin: 'AC',
      destination: 'AL',
      tariff: 6,
    },
    {
      origin: 'AC',
      destination: 'AM',
      tariff: 3,
    },
    {
      origin: 'AC',
      destination: 'AP',
      tariff: 5,
    },
    {
      origin: 'AC',
      destination: 'BA',
      tariff: 6,
    },
    {
      origin: 'AC',
      destination: 'CE',
      tariff: 6,
    },
    {
      origin: 'AC',
      destination: 'DF',
      tariff: 5,
    },
    {
      origin: 'AC',
      destination: 'ES',
      tariff: 6,
    },
    {
      origin: 'AC',
      destination: 'GO',
      tariff: 5,
    },
    {
      origin: 'AC',
      destination: 'MA',
      tariff: 5,
    },
    {
      origin: 'AC',
      destination: 'MG',
      tariff: 5,
    },
    {
      origin: 'AC',
      destination: 'MS',
      tariff: 3,
    },
    {
      origin: 'AC',
      destination: 'MT',
      tariff: 4,
    },
    {
      origin: 'AC',
      destination: 'PA',
      tariff: 5,
    },
    {
      origin: 'AC',
      destination: 'PB',
      tariff: 6,
    },
    {
      origin: 'AC',
      destination: 'PE',
      tariff: 6,
    },
    {
      origin: 'AC',
      destination: 'PI',
      tariff: 5,
    },
    {
      origin: 'AC',
      destination: 'PR',
      tariff: 5,
    },
    {
      origin: 'AC',
      destination: 'RJ',
      tariff: 6,
    },
    {
      origin: 'AC',
      destination: 'RN',
      tariff: 6,
    },
    {
      origin: 'AC',
      destination: 'RO',
      tariff: 1,
    },
    {
      origin: 'AC',
      destination: 'RR',
      tariff: 4,
    },
    {
      origin: 'AC',
      destination: 'RS',
      tariff: 5,
    },
    {
      origin: 'AC',
      destination: 'SC',
      tariff: 5,
    },
    {
      origin: 'AC',
      destination: 'SE',
      tariff: 6,
    },
    {
      origin: 'AC',
      destination: 'SP',
      tariff: 5,
    },
    {
      origin: 'AC',
      destination: 'TO',
      tariff: 5,
    },
    {
      origin: 'AL',
      destination: 'AC',
      tariff: 6,
    },
    {
      origin: 'AL',
      destination: 'AL',
      tariff: 4,
    },
    {
      origin: 'AL',
      destination: 'AM',
      tariff: 6,
    },
    {
      origin: 'AL',
      destination: 'AP',
      tariff: 5,
    },
    {
      origin: 'AL',
      destination: 'BA',
      tariff: 1,
    },
    {
      origin: 'AL',
      destination: 'CE',
      tariff: 2,
    },
    {
      origin: 'AL',
      destination: 'DF',
      tariff: 4,
    },
    {
      origin: 'AL',
      destination: 'ES',
      tariff: 3,
    },
    {
      origin: 'AL',
      destination: 'MA',
      tariff: 3,
    },
    {
      origin: 'AL',
      destination: 'MG',
      tariff: 4,
    },
    {
      origin: 'AL',
      destination: 'MS',
      tariff: 4,
    },
    {
      origin: 'AL',
      destination: 'MT',
      tariff: 5,
    },
    {
      origin: 'AL',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'AL',
      destination: 'PB',
      tariff: 1,
    },
    {
      origin: 'AL',
      destination: 'PE',
      tariff: 1,
    },
    {
      origin: 'AL',
      destination: 'PI',
      tariff: 2,
    },
    {
      origin: 'AL',
      destination: 'PR',
      tariff: 5,
    },
    {
      origin: 'AL',
      destination: 'RJ',
      tariff: 4,
    },
    {
      origin: 'AL',
      destination: 'RN',
      tariff: 1,
    },
    {
      origin: 'AL',
      destination: 'RO',
      tariff: 6,
    },
    {
      origin: 'AL',
      destination: 'RR',
      tariff: 6,
    },
    {
      origin: 'AL',
      destination: 'RS',
      tariff: 5,
    },
    {
      origin: 'AL',
      destination: 'SC',
      tariff: 5,
    },
    {
      origin: 'AL',
      destination: 'SE',
      tariff: 1,
    },
    {
      origin: 'AL',
      destination: 'SP',
      tariff: 4,
    },
    {
      origin: 'AL',
      destination: 'TO',
      tariff: 5,
    },
    {
      origin: 'AL',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'AM',
      destination: 'AC',
      tariff: 3,
    },
    {
      origin: 'AM',
      destination: 'AL',
      tariff: 6,
    },
    {
      origin: 'AM',
      destination: 'AM',
      tariff: 4,
    },
    {
      origin: 'AM',
      destination: 'AP',
      tariff: 4,
    },
    {
      origin: 'AM',
      destination: 'BA',
      tariff: 5,
    },
    {
      origin: 'AM',
      destination: 'CE',
      tariff: 5,
    },
    {
      origin: 'AM',
      destination: 'DF',
      tariff: 4,
    },
    {
      origin: 'AM',
      destination: 'ES',
      tariff: 5,
    },
    {
      origin: 'AM',
      destination: 'GO',
      tariff: 5,
    },
    {
      origin: 'AM',
      destination: 'MA',
      tariff: 4,
    },
    {
      origin: 'AM',
      destination: 'MG',
      tariff: 5,
    },
    {
      origin: 'AM',
      destination: 'MS',
      tariff: 4,
    },
    {
      origin: 'AM',
      destination: 'MT',
      tariff: 4,
    },
    {
      origin: 'AM',
      destination: 'PA',
      tariff: 3,
    },
    {
      origin: 'AM',
      destination: 'PB',
      tariff: 5,
    },
    {
      origin: 'AM',
      destination: 'PE',
      tariff: 5,
    },
    {
      origin: 'AM',
      destination: 'PI',
      tariff: 4,
    },
    {
      origin: 'AM',
      destination: 'PR',
      tariff: 5,
    },
    {
      origin: 'AM',
      destination: 'RJ',
      tariff: 5,
    },
    {
      origin: 'AM',
      destination: 'RN',
      tariff: 5,
    },
    {
      origin: 'AM',
      destination: 'RO',
      tariff: 2,
    },
    {
      origin: 'AM',
      destination: 'RR',
      tariff: 2,
    },
    {
      origin: 'AM',
      destination: 'RS',
      tariff: 6,
    },
    {
      origin: 'AM',
      destination: 'SC',
      tariff: 6,
    },
    {
      origin: 'AM',
      destination: 'SE',
      tariff: 5,
    },
    {
      origin: 'AM',
      destination: 'SP',
      tariff: 5,
    },
    {
      origin: 'AM',
      destination: 'TO',
      tariff: 4,
    },
    {
      origin: 'AP',
      destination: 'AC',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'AL',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'AM',
      tariff: 4,
    },
    {
      origin: 'AP',
      destination: 'AP',
      tariff: 4,
    },
    {
      origin: 'AP',
      destination: 'BA',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'CE',
      tariff: 4,
    },
    {
      origin: 'AP',
      destination: 'DF',
      tariff: 4,
    },
    {
      origin: 'AP',
      destination: 'ES',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'GO',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'MA',
      tariff: 3,
    },
    {
      origin: 'AP',
      destination: 'MG',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'MS',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'MT',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'PA',
      tariff: 1,
    },
    {
      origin: 'AP',
      destination: 'PB',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'PE',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'PI',
      tariff: 3,
    },
    {
      origin: 'AP',
      destination: 'PR',
      tariff: 6,
    },
    {
      origin: 'AP',
      destination: 'RJ',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'RN',
      tariff: 4,
    },
    {
      origin: 'AP',
      destination: 'RO',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'RR',
      tariff: 4,
    },
    {
      origin: 'AP',
      destination: 'RS',
      tariff: 6,
    },
    {
      origin: 'AP',
      destination: 'SC',
      tariff: 6,
    },
    {
      origin: 'AP',
      destination: 'SE',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'SP',
      tariff: 5,
    },
    {
      origin: 'AP',
      destination: 'TO',
      tariff: 3,
    },
    {
      origin: 'BA',
      destination: 'AC',
      tariff: 6,
    },
    {
      origin: 'BA',
      destination: 'AL',
      tariff: 1,
    },
    {
      origin: 'BA',
      destination: 'AM',
      tariff: 5,
    },
    {
      origin: 'BA',
      destination: 'AP',
      tariff: 5,
    },
    {
      origin: 'BA',
      destination: 'BA',
      tariff: 4,
    },
    {
      origin: 'BA',
      destination: 'CE',
      tariff: 3,
    },
    {
      origin: 'BA',
      destination: 'DF',
      tariff: 3,
    },
    {
      origin: 'BA',
      destination: 'ES',
      tariff: 2,
    },
    {
      origin: 'BA',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'BA',
      destination: 'MA',
      tariff: 3,
    },
    {
      origin: 'BA',
      destination: 'MG',
      tariff: 3,
    },
    {
      origin: 'BA',
      destination: 'MS',
      tariff: 4,
    },
    {
      origin: 'BA',
      destination: 'MT',
      tariff: 4,
    },
    {
      origin: 'BA',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'BA',
      destination: 'PB',
      tariff: 2,
    },
    {
      origin: 'BA',
      destination: 'PE',
      tariff: 2,
    },
    {
      origin: 'BA',
      destination: 'PI',
      tariff: 3,
    },
    {
      origin: 'BA',
      destination: 'PR',
      tariff: 4,
    },
    {
      origin: 'BA',
      destination: 'RJ',
      tariff: 3,
    },
    {
      origin: 'BA',
      destination: 'RN',
      tariff: 2,
    },
    {
      origin: 'BA',
      destination: 'RO',
      tariff: 5,
    },
    {
      origin: 'BA',
      destination: 'RR',
      tariff: 6,
    },
    {
      origin: 'BA',
      destination: 'RS',
      tariff: 5,
    },
    {
      origin: 'BA',
      destination: 'SC',
      tariff: 4,
    },
    {
      origin: 'BA',
      destination: 'SE',
      tariff: 1,
    },
    {
      origin: 'BA',
      destination: 'SP',
      tariff: 4,
    },
    {
      origin: 'BA',
      destination: 'TO',
      tariff: 4,
    },
    {
      origin: 'CE',
      destination: 'AC',
      tariff: 6,
    },
    {
      origin: 'CE',
      destination: 'AL',
      tariff: 2,
    },
    {
      origin: 'CE',
      destination: 'AM',
      tariff: 5,
    },
    {
      origin: 'CE',
      destination: 'AP',
      tariff: 4,
    },
    {
      origin: 'CE',
      destination: 'BA',
      tariff: 3,
    },
    {
      origin: 'CE',
      destination: 'CE',
      tariff: 4,
    },
    {
      origin: 'CE',
      destination: 'DF',
      tariff: 4,
    },
    {
      origin: 'CE',
      destination: 'ES',
      tariff: 4,
    },
    {
      origin: 'CE',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'CE',
      destination: 'MA',
      tariff: 1,
    },
    {
      origin: 'CE',
      destination: 'MG',
      tariff: 4,
    },
    {
      origin: 'CE',
      destination: 'MS',
      tariff: 5,
    },
    {
      origin: 'CE',
      destination: 'MT',
      tariff: 5,
    },
    {
      origin: 'CE',
      destination: 'PA',
      tariff: 3,
    },
    {
      origin: 'CE',
      destination: 'PB',
      tariff: 1,
    },
    {
      origin: 'CE',
      destination: 'PE',
      tariff: 1,
    },
    {
      origin: 'CE',
      destination: 'PI',
      tariff: 1,
    },
    {
      origin: 'CE',
      destination: 'PR',
      tariff: 5,
    },
    {
      origin: 'CE',
      destination: 'RJ',
      tariff: 5,
    },
    {
      origin: 'CE',
      destination: 'RN',
      tariff: 1,
    },
    {
      origin: 'CE',
      destination: 'RO',
      tariff: 5,
    },
    {
      origin: 'CE',
      destination: 'RR',
      tariff: 5,
    },
    {
      origin: 'CE',
      destination: 'RS',
      tariff: 6,
    },
    {
      origin: 'CE',
      destination: 'SC',
      tariff: 6,
    },
    {
      origin: 'CE',
      destination: 'SE',
      tariff: 2,
    },
    {
      origin: 'CE',
      destination: 'SP',
      tariff: 5,
    },
    {
      origin: 'CE',
      destination: 'TO',
      tariff: 5,
    },
    {
      origin: 'DF',
      destination: 'AC',
      tariff: 5,
    },
    {
      origin: 'DF',
      destination: 'AL',
      tariff: 4,
    },
    {
      origin: 'DF',
      destination: 'AM',
      tariff: 4,
    },
    {
      origin: 'DF',
      destination: 'AP',
      tariff: 4,
    },
    {
      origin: 'DF',
      destination: 'BA',
      tariff: 3,
    },
    {
      origin: 'DF',
      destination: 'CE',
      tariff: 4,
    },
    {
      origin: 'DF',
      destination: 'DF',
      tariff: 3,
    },
    {
      origin: 'DF',
      destination: 'ES',
      tariff: 3,
    },
    {
      origin: 'DF',
      destination: 'GO',
      tariff: 1,
    },
    {
      origin: 'DF',
      destination: 'MA',
      tariff: 4,
    },
    {
      origin: 'DF',
      destination: 'MG',
      tariff: 1,
    },
    {
      origin: 'DF',
      destination: 'MS',
      tariff: 2,
    },
    {
      origin: 'DF',
      destination: 'MT',
      tariff: 2,
    },
    {
      origin: 'DF',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'DF',
      destination: 'PB',
      tariff: 4,
    },
    {
      origin: 'DF',
      destination: 'PE',
      tariff: 4,
    },
    {
      origin: 'DF',
      destination: 'PI',
      tariff: 3,
    },
    {
      origin: 'DF',
      destination: 'PR',
      tariff: 3,
    },
    {
      origin: 'DF',
      destination: 'RJ',
      tariff: 2,
    },
    {
      origin: 'DF',
      destination: 'RN',
      tariff: 4,
    },
    {
      origin: 'DF',
      destination: 'RO',
      tariff: 4,
    },
    {
      origin: 'DF',
      destination: 'RR',
      tariff: 5,
    },
    {
      origin: 'DF',
      destination: 'RS',
      tariff: 4,
    },
    {
      origin: 'DF',
      destination: 'SC',
      tariff: 3,
    },
    {
      origin: 'DF',
      destination: 'SE',
      tariff: 3,
    },
    {
      origin: 'DF',
      destination: 'SP',
      tariff: 2,
    },
    {
      origin: 'DF',
      destination: 'TO',
      tariff: 1,
    },
    {
      origin: 'ES',
      destination: 'AC',
      tariff: 6,
    },
    {
      origin: 'ES',
      destination: 'AL',
      tariff: 3,
    },
    {
      origin: 'ES',
      destination: 'AM',
      tariff: 5,
    },
    {
      origin: 'ES',
      destination: 'AP',
      tariff: 5,
    },
    {
      origin: 'ES',
      destination: 'BA',
      tariff: 2,
    },
    {
      origin: 'ES',
      destination: 'CE',
      tariff: 4,
    },
    {
      origin: 'ES',
      destination: 'DF',
      tariff: 3,
    },
    {
      origin: 'ES',
      destination: 'ES',
      tariff: 4,
    },
    {
      origin: 'ES',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'ES',
      destination: 'MA',
      tariff: 4,
    },
    {
      origin: 'ES',
      destination: 'MG',
      tariff: 1,
    },
    {
      origin: 'ES',
      destination: 'MS',
      tariff: 3,
    },
    {
      origin: 'ES',
      destination: 'MT',
      tariff: 4,
    },
    {
      origin: 'ES',
      destination: 'PA',
      tariff: 5,
    },
    {
      origin: 'ES',
      destination: 'PB',
      tariff: 4,
    },
    {
      origin: 'ES',
      destination: 'PE',
      tariff: 4,
    },
    {
      origin: 'ES',
      destination: 'PI',
      tariff: 4,
    },
    {
      origin: 'ES',
      destination: 'PR',
      tariff: 3,
    },
    {
      origin: 'ES',
      destination: 'RJ',
      tariff: 1,
    },
    {
      origin: 'ES',
      destination: 'RN',
      tariff: 4,
    },
    {
      origin: 'ES',
      destination: 'RO',
      tariff: 5,
    },
    {
      origin: 'ES',
      destination: 'RR',
      tariff: 6,
    },
    {
      origin: 'ES',
      destination: 'RS',
      tariff: 4,
    },
    {
      origin: 'ES',
      destination: 'SC',
      tariff: 3,
    },
    {
      origin: 'ES',
      destination: 'SE',
      tariff: 3,
    },
    {
      origin: 'ES',
      destination: 'SP',
      tariff: 2,
    },
    {
      origin: 'ES',
      destination: 'TO',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'AC',
      tariff: 5,
    },
    {
      origin: 'GO',
      destination: 'AL',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'AM',
      tariff: 5,
    },
    {
      origin: 'GO',
      destination: 'AP',
      tariff: 5,
    },
    {
      origin: 'GO',
      destination: 'BA',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'CE',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'DF',
      tariff: 1,
    },
    {
      origin: 'GO',
      destination: 'ES',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'MA',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'MG',
      tariff: 2,
    },
    {
      origin: 'GO',
      destination: 'MS',
      tariff: 2,
    },
    {
      origin: 'GO',
      destination: 'MT',
      tariff: 2,
    },
    {
      origin: 'GO',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'PB',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'PE',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'PI',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'PR',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'RJ',
      tariff: 3,
    },
    {
      origin: 'GO',
      destination: 'RN',
      tariff: 5,
    },
    {
      origin: 'GO',
      destination: 'RO',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'RR',
      tariff: 5,
    },
    {
      origin: 'GO',
      destination: 'RS',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'SC',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'SE',
      tariff: 4,
    },
    {
      origin: 'GO',
      destination: 'SP',
      tariff: 3,
    },
    {
      origin: 'GO',
      destination: 'TO',
      tariff: 1,
    },
    {
      origin: 'MA',
      destination: 'AC',
      tariff: 5,
    },
    {
      origin: 'MA',
      destination: 'AL',
      tariff: 3,
    },
    {
      origin: 'MA',
      destination: 'AM',
      tariff: 4,
    },
    {
      origin: 'MA',
      destination: 'AP',
      tariff: 3,
    },
    {
      origin: 'MA',
      destination: 'BA',
      tariff: 3,
    },
    {
      origin: 'MA',
      destination: 'CE',
      tariff: 1,
    },
    {
      origin: 'MA',
      destination: 'DF',
      tariff: 4,
    },
    {
      origin: 'MA',
      destination: 'ES',
      tariff: 4,
    },
    {
      origin: 'MA',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'MA',
      destination: 'MA',
      tariff: 4,
    },
    {
      origin: 'MA',
      destination: 'MG',
      tariff: 4,
    },
    {
      origin: 'MA',
      destination: 'MS',
      tariff: 5,
    },
    {
      origin: 'MA',
      destination: 'MT',
      tariff: 4,
    },
    {
      origin: 'MA',
      destination: 'PA',
      tariff: 1,
    },
    {
      origin: 'MA',
      destination: 'PB',
      tariff: 3,
    },
    {
      origin: 'MA',
      destination: 'PE',
      tariff: 3,
    },
    {
      origin: 'MA',
      destination: 'PI',
      tariff: 1,
    },
    {
      origin: 'MA',
      destination: 'PR',
      tariff: 5,
    },
    {
      origin: 'MA',
      destination: 'RJ',
      tariff: 5,
    },
    {
      origin: 'MA',
      destination: 'RN',
      tariff: 3,
    },
    {
      origin: 'MA',
      destination: 'RO',
      tariff: 5,
    },
    {
      origin: 'MA',
      destination: 'RR',
      tariff: 4,
    },
    {
      origin: 'MA',
      destination: 'RS',
      tariff: 6,
    },
    {
      origin: 'MA',
      destination: 'SC',
      tariff: 5,
    },
    {
      origin: 'MA',
      destination: 'SE',
      tariff: 3,
    },
    {
      origin: 'MA',
      destination: 'SP',
      tariff: 5,
    },
    {
      origin: 'MA',
      destination: 'TO',
      tariff: 5,
    },
    {
      origin: 'MG',
      destination: 'AC',
      tariff: 6,
    },
    {
      origin: 'MG',
      destination: 'AL',
      tariff: 4,
    },
    {
      origin: 'MG',
      destination: 'AM',
      tariff: 5,
    },
    {
      origin: 'MG',
      destination: 'AP',
      tariff: 6,
    },
    {
      origin: 'MG',
      destination: 'BA',
      tariff: 2,
    },
    {
      origin: 'MG',
      destination: 'CE',
      tariff: 4,
    },
    {
      origin: 'MG',
      destination: 'DF',
      tariff: 1,
    },
    {
      origin: 'MG',
      destination: 'ES',
      tariff: 2,
    },
    {
      origin: 'MG',
      destination: 'GO',
      tariff: 2,
    },
    {
      origin: 'MG',
      destination: 'MA',
      tariff: 4,
    },
    {
      origin: 'MG',
      destination: 'MG',
      tariff: 3,
    },
    {
      origin: 'MG',
      destination: 'MS',
      tariff: 2,
    },
    {
      origin: 'MG',
      destination: 'MT',
      tariff: 3,
    },
    {
      origin: 'MG',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'MG',
      destination: 'PB',
      tariff: 4,
    },
    {
      origin: 'MG',
      destination: 'PE',
      tariff: 4,
    },
    {
      origin: 'MG',
      destination: 'PI',
      tariff: 5,
    },
    {
      origin: 'MG',
      destination: 'PR',
      tariff: 2,
    },
    {
      origin: 'MG',
      destination: 'RJ',
      tariff: 1,
    },
    {
      origin: 'MG',
      destination: 'RN',
      tariff: 5,
    },
    {
      origin: 'MG',
      destination: 'RO',
      tariff: 5,
    },
    {
      origin: 'MG',
      destination: 'RR',
      tariff: 6,
    },
    {
      origin: 'MG',
      destination: 'RS',
      tariff: 3,
    },
    {
      origin: 'MG',
      destination: 'SC',
      tariff: 2,
    },
    {
      origin: 'MG',
      destination: 'SE',
      tariff: 4,
    },
    {
      origin: 'MG',
      destination: 'SP',
      tariff: 1,
    },
    {
      origin: 'MG',
      destination: 'TO',
      tariff: 4,
    },
    {
      origin: 'MS',
      destination: 'AC',
      tariff: 3,
    },
    {
      origin: 'MS',
      destination: 'AL',
      tariff: 4,
    },
    {
      origin: 'MS',
      destination: 'AM',
      tariff: 4,
    },
    {
      origin: 'MS',
      destination: 'AP',
      tariff: 5,
    },
    {
      origin: 'MS',
      destination: 'BA',
      tariff: 4,
    },
    {
      origin: 'MS',
      destination: 'CE',
      tariff: 5,
    },
    {
      origin: 'MS',
      destination: 'DF',
      tariff: 2,
    },
    {
      origin: 'MS',
      destination: 'ES',
      tariff: 3,
    },
    {
      origin: 'MS',
      destination: 'GO',
      tariff: 2,
    },
    {
      origin: 'MS',
      destination: 'MA',
      tariff: 5,
    },
    {
      origin: 'MS',
      destination: 'MG',
      tariff: 2,
    },
    {
      origin: 'MS',
      destination: 'MS',
      tariff: 4,
    },
    {
      origin: 'MS',
      destination: 'MT',
      tariff: 1,
    },
    {
      origin: 'MS',
      destination: 'PA',
      tariff: 5,
    },
    {
      origin: 'MS',
      destination: 'PB',
      tariff: 5,
    },
    {
      origin: 'MS',
      destination: 'PE',
      tariff: 5,
    },
    {
      origin: 'MS',
      destination: 'PI',
      tariff: 5,
    },
    {
      origin: 'MS',
      destination: 'PR',
      tariff: 2,
    },
    {
      origin: 'MS',
      destination: 'RJ',
      tariff: 2,
    },
    {
      origin: 'MS',
      destination: 'RN',
      tariff: 5,
    },
    {
      origin: 'MS',
      destination: 'RO',
      tariff: 3,
    },
    {
      origin: 'MS',
      destination: 'RR',
      tariff: 5,
    },
    {
      origin: 'MS',
      destination: 'RS',
      tariff: 3,
    },
    {
      origin: 'MS',
      destination: 'SC',
      tariff: 2,
    },
    {
      origin: 'MS',
      destination: 'SE',
      tariff: 4,
    },
    {
      origin: 'MS',
      destination: 'SP',
      tariff: 2,
    },
    {
      origin: 'MS',
      destination: 'TO',
      tariff: 3,
    },
    {
      origin: 'MT',
      destination: 'AC',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'AL',
      tariff: 5,
    },
    {
      origin: 'MT',
      destination: 'AM',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'AP',
      tariff: 5,
    },
    {
      origin: 'MT',
      destination: 'BA',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'CE',
      tariff: 5,
    },
    {
      origin: 'MT',
      destination: 'DF',
      tariff: 2,
    },
    {
      origin: 'MT',
      destination: 'ES',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'GO',
      tariff: 2,
    },
    {
      origin: 'MT',
      destination: 'MA',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'MG',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'MS',
      tariff: 1,
    },
    {
      origin: 'MT',
      destination: 'MT',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'PB',
      tariff: 5,
    },
    {
      origin: 'MT',
      destination: 'PE',
      tariff: 5,
    },
    {
      origin: 'MT',
      destination: 'PI',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'PR',
      tariff: 3,
    },
    {
      origin: 'MT',
      destination: 'RJ',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'RN',
      tariff: 5,
    },
    {
      origin: 'MT',
      destination: 'RO',
      tariff: 3,
    },
    {
      origin: 'MT',
      destination: 'RR',
      tariff: 5,
    },
    {
      origin: 'MT',
      destination: 'RS',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'SC',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'SE',
      tariff: 5,
    },
    {
      origin: 'MT',
      destination: 'SP',
      tariff: 4,
    },
    {
      origin: 'MT',
      destination: 'TO',
      tariff: 3,
    },
    {
      origin: 'PA',
      destination: 'AC',
      tariff: 5,
    },
    {
      origin: 'PA',
      destination: 'AL',
      tariff: 4,
    },
    {
      origin: 'PA',
      destination: 'AM',
      tariff: 3,
    },
    {
      origin: 'PA',
      destination: 'AP',
      tariff: 1,
    },
    {
      origin: 'PA',
      destination: 'BA',
      tariff: 4,
    },
    {
      origin: 'PA',
      destination: 'CE',
      tariff: 3,
    },
    {
      origin: 'PA',
      destination: 'DF',
      tariff: 4,
    },
    {
      origin: 'PA',
      destination: 'ES',
      tariff: 5,
    },
    {
      origin: 'PA',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'PA',
      destination: 'MA',
      tariff: 1,
    },
    {
      origin: 'PA',
      destination: 'MG',
      tariff: 5,
    },
    {
      origin: 'PA',
      destination: 'MS',
      tariff: 5,
    },
    {
      origin: 'PA',
      destination: 'MT',
      tariff: 4,
    },
    {
      origin: 'PA',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'PA',
      destination: 'PB',
      tariff: 4,
    },
    {
      origin: 'PA',
      destination: 'PE',
      tariff: 4,
    },
    {
      origin: 'PA',
      destination: 'PI',
      tariff: 2,
    },
    {
      origin: 'PA',
      destination: 'PR',
      tariff: 5,
    },
    {
      origin: 'PA',
      destination: 'RJ',
      tariff: 5,
    },
    {
      origin: 'PA',
      destination: 'RN',
      tariff: 4,
    },
    {
      origin: 'PA',
      destination: 'RO',
      tariff: 4,
    },
    {
      origin: 'PA',
      destination: 'RR',
      tariff: 4,
    },
    {
      origin: 'PA',
      destination: 'RS',
      tariff: 6,
    },
    {
      origin: 'PA',
      destination: 'SC',
      tariff: 6,
    },
    {
      origin: 'PA',
      destination: 'SE',
      tariff: 4,
    },
    {
      origin: 'PA',
      destination: 'SP',
      tariff: 5,
    },
    {
      origin: 'PA',
      destination: 'TO',
      tariff: 2,
    },
    {
      origin: 'PB',
      destination: 'AC',
      tariff: 6,
    },
    {
      origin: 'PB',
      destination: 'AL',
      tariff: 1,
    },
    {
      origin: 'PB',
      destination: 'AM',
      tariff: 5,
    },
    {
      origin: 'PB',
      destination: 'AP',
      tariff: 5,
    },
    {
      origin: 'PB',
      destination: 'BA',
      tariff: 2,
    },
    {
      origin: 'PB',
      destination: 'CE',
      tariff: 1,
    },
    {
      origin: 'PB',
      destination: 'DF',
      tariff: 4,
    },
    {
      origin: 'PB',
      destination: 'ES',
      tariff: 4,
    },
    {
      origin: 'PB',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'PB',
      destination: 'MA',
      tariff: 3,
    },
    {
      origin: 'PB',
      destination: 'MG',
      tariff: 4,
    },
    {
      origin: 'PB',
      destination: 'MS',
      tariff: 5,
    },
    {
      origin: 'PB',
      destination: 'MT',
      tariff: 5,
    },
    {
      origin: 'PB',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'PB',
      destination: 'PB',
      tariff: 4,
    },
    {
      origin: 'PB',
      destination: 'PE',
      tariff: 1,
    },
    {
      origin: 'PB',
      destination: 'PI',
      tariff: 2,
    },
    {
      origin: 'PB',
      destination: 'PR',
      tariff: 5,
    },
    {
      origin: 'PB',
      destination: 'RJ',
      tariff: 4,
    },
    {
      origin: 'PB',
      destination: 'RN',
      tariff: 1,
    },
    {
      origin: 'PB',
      destination: 'RO',
      tariff: 6,
    },
    {
      origin: 'PB',
      destination: 'RR',
      tariff: 6,
    },
    {
      origin: 'PB',
      destination: 'RS',
      tariff: 6,
    },
    {
      origin: 'PB',
      destination: 'SC',
      tariff: 5,
    },
    {
      origin: 'PB',
      destination: 'SE',
      tariff: 1,
    },
    {
      origin: 'PB',
      destination: 'SP',
      tariff: 5,
    },
    {
      origin: 'PB',
      destination: 'TO',
      tariff: 5,
    },
    {
      origin: 'PE',
      destination: 'AC',
      tariff: 6,
    },
    {
      origin: 'PE',
      destination: 'AL',
      tariff: 1,
    },
    {
      origin: 'PE',
      destination: 'AM',
      tariff: 5,
    },
    {
      origin: 'PE',
      destination: 'AP',
      tariff: 5,
    },
    {
      origin: 'PE',
      destination: 'BA',
      tariff: 2,
    },
    {
      origin: 'PE',
      destination: 'CE',
      tariff: 1,
    },
    {
      origin: 'PE',
      destination: 'DF',
      tariff: 4,
    },
    {
      origin: 'PE',
      destination: 'ES',
      tariff: 4,
    },
    {
      origin: 'PE',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'PE',
      destination: 'MA',
      tariff: 3,
    },
    {
      origin: 'PE',
      destination: 'MG',
      tariff: 4,
    },
    {
      origin: 'PE',
      destination: 'MS',
      tariff: 5,
    },
    {
      origin: 'PE',
      destination: 'MT',
      tariff: 5,
    },
    {
      origin: 'PE',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'PE',
      destination: 'PB',
      tariff: 1,
    },
    {
      origin: 'PE',
      destination: 'PE',
      tariff: 4,
    },
    {
      origin: 'PE',
      destination: 'PI',
      tariff: 2,
    },
    {
      origin: 'PE',
      destination: 'PR',
      tariff: 5,
    },
    {
      origin: 'PE',
      destination: 'RJ',
      tariff: 4,
    },
    {
      origin: 'PE',
      destination: 'RN',
      tariff: 1,
    },
    {
      origin: 'PE',
      destination: 'RO',
      tariff: 6,
    },
    {
      origin: 'PE',
      destination: 'RR',
      tariff: 6,
    },
    {
      origin: 'PE',
      destination: 'RS',
      tariff: 6,
    },
    {
      origin: 'PE',
      destination: 'SC',
      tariff: 5,
    },
    {
      origin: 'PE',
      destination: 'SE',
      tariff: 1,
    },
    {
      origin: 'PE',
      destination: 'SP',
      tariff: 5,
    },
    {
      origin: 'PE',
      destination: 'TO',
      tariff: 4,
    },
    {
      origin: 'PI',
      destination: 'AC',
      tariff: 5,
    },
    {
      origin: 'PI',
      destination: 'AL',
      tariff: 2,
    },
    {
      origin: 'PI',
      destination: 'AM',
      tariff: 4,
    },
    {
      origin: 'PI',
      destination: 'AP',
      tariff: 3,
    },
    {
      origin: 'PI',
      destination: 'BA',
      tariff: 3,
    },
    {
      origin: 'PI',
      destination: 'CE',
      tariff: 1,
    },
    {
      origin: 'PI',
      destination: 'DF',
      tariff: 3,
    },
    {
      origin: 'PI',
      destination: 'ES',
      tariff: 4,
    },
    {
      origin: 'PI',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'PI',
      destination: 'MA',
      tariff: 1,
    },
    {
      origin: 'PI',
      destination: 'MG',
      tariff: 4,
    },
    {
      origin: 'PI',
      destination: 'MS',
      tariff: 5,
    },
    {
      origin: 'PI',
      destination: 'MT',
      tariff: 4,
    },
    {
      origin: 'PI',
      destination: 'PA',
      tariff: 2,
    },
    {
      origin: 'PI',
      destination: 'PB',
      tariff: 2,
    },
    {
      origin: 'PI',
      destination: 'PE',
      tariff: 2,
    },
    {
      origin: 'PI',
      destination: 'PI',
      tariff: 4,
    },
    {
      origin: 'PI',
      destination: 'PR',
      tariff: 5,
    },
    {
      origin: 'PI',
      destination: 'RJ',
      tariff: 4,
    },
    {
      origin: 'PI',
      destination: 'RN',
      tariff: 2,
    },
    {
      origin: 'PI',
      destination: 'RO',
      tariff: 5,
    },
    {
      origin: 'PI',
      destination: 'RR',
      tariff: 5,
    },
    {
      origin: 'PI',
      destination: 'RS',
      tariff: 6,
    },
    {
      origin: 'PI',
      destination: 'SC',
      tariff: 5,
    },
    {
      origin: 'PI',
      destination: 'SE',
      tariff: 4,
    },
    {
      origin: 'PI',
      destination: 'SP',
      tariff: 5,
    },
    {
      origin: 'PI',
      destination: 'TO',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'AC',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'AL',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'AM',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'AP',
      tariff: 6,
    },
    {
      origin: 'PR',
      destination: 'BA',
      tariff: 4,
    },
    {
      origin: 'PR',
      destination: 'CE',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'DF',
      tariff: 3,
    },
    {
      origin: 'PR',
      destination: 'ES',
      tariff: 3,
    },
    {
      origin: 'PR',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'PR',
      destination: 'MA',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'MG',
      tariff: 2,
    },
    {
      origin: 'PR',
      destination: 'MS',
      tariff: 2,
    },
    {
      origin: 'PR',
      destination: 'MT',
      tariff: 3,
    },
    {
      origin: 'PR',
      destination: 'PA',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'PB',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'PE',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'PI',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'PR',
      tariff: 3,
    },
    {
      origin: 'PR',
      destination: 'RJ',
      tariff: 2,
    },
    {
      origin: 'PR',
      destination: 'RN',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'RO',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'RR',
      tariff: 6,
    },
    {
      origin: 'PR',
      destination: 'RS',
      tariff: 1,
    },
    {
      origin: 'PR',
      destination: 'SC',
      tariff: 1,
    },
    {
      origin: 'PR',
      destination: 'SE',
      tariff: 5,
    },
    {
      origin: 'PR',
      destination: 'SP',
      tariff: 1,
    },
    {
      origin: 'PR',
      destination: 'TO',
      tariff: 4,
    },
    {
      origin: 'RJ',
      destination: 'AC',
      tariff: 6,
    },
    {
      origin: 'RJ',
      destination: 'AL',
      tariff: 4,
    },
    {
      origin: 'RJ',
      destination: 'AM',
      tariff: 5,
    },
    {
      origin: 'RJ',
      destination: 'AP',
      tariff: 5,
    },
    {
      origin: 'RJ',
      destination: 'BA',
      tariff: 3,
    },
    {
      origin: 'RJ',
      destination: 'CE',
      tariff: 4,
    },
    {
      origin: 'RJ',
      destination: 'DF',
      tariff: 1,
    },
    {
      origin: 'RJ',
      destination: 'ES',
      tariff: 1,
    },
    {
      origin: 'RJ',
      destination: 'GO',
      tariff: 2,
    },
    {
      origin: 'RJ',
      destination: 'MA',
      tariff: 4,
    },
    {
      origin: 'RJ',
      destination: 'MG',
      tariff: 1,
    },
    {
      origin: 'RJ',
      destination: 'MS',
      tariff: 2,
    },
    {
      origin: 'RJ',
      destination: 'MT',
      tariff: 3,
    },
    {
      origin: 'RJ',
      destination: 'PA',
      tariff: 5,
    },
    {
      origin: 'RJ',
      destination: 'PB',
      tariff: 4,
    },
    {
      origin: 'RJ',
      destination: 'PE',
      tariff: 4,
    },
    {
      origin: 'RJ',
      destination: 'PI',
      tariff: 4,
    },
    {
      origin: 'RJ',
      destination: 'PR',
      tariff: 1,
    },
    {
      origin: 'RJ',
      destination: 'RJ',
      tariff: 2,
    },
    {
      origin: 'RJ',
      destination: 'RN',
      tariff: 4,
    },
    {
      origin: 'RJ',
      destination: 'RO',
      tariff: 4,
    },
    {
      origin: 'RJ',
      destination: 'RR',
      tariff: 6,
    },
    {
      origin: 'RJ',
      destination: 'RS',
      tariff: 2,
    },
    {
      origin: 'RJ',
      destination: 'SC',
      tariff: 2,
    },
    {
      origin: 'RJ',
      destination: 'SE',
      tariff: 3,
    },
    {
      origin: 'RJ',
      destination: 'SP',
      tariff: 1,
    },
    {
      origin: 'RJ',
      destination: 'TO',
      tariff: 3,
    },
    {
      origin: 'RN',
      destination: 'AC',
      tariff: 6,
    },
    {
      origin: 'RN',
      destination: 'AL',
      tariff: 1,
    },
    {
      origin: 'RN',
      destination: 'AM',
      tariff: 5,
    },
    {
      origin: 'RN',
      destination: 'AP',
      tariff: 4,
    },
    {
      origin: 'RN',
      destination: 'BA',
      tariff: 2,
    },
    {
      origin: 'RN',
      destination: 'CE',
      tariff: 1,
    },
    {
      origin: 'RN',
      destination: 'DF',
      tariff: 4,
    },
    {
      origin: 'RN',
      destination: 'ES',
      tariff: 4,
    },
    {
      origin: 'RN',
      destination: 'GO',
      tariff: 5,
    },
    {
      origin: 'RN',
      destination: 'MA',
      tariff: 3,
    },
    {
      origin: 'RN',
      destination: 'MG',
      tariff: 4,
    },
    {
      origin: 'RN',
      destination: 'MS',
      tariff: 5,
    },
    {
      origin: 'RN',
      destination: 'MT',
      tariff: 5,
    },
    {
      origin: 'RN',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'RN',
      destination: 'PB',
      tariff: 1,
    },
    {
      origin: 'RN',
      destination: 'PE',
      tariff: 1,
    },
    {
      origin: 'RN',
      destination: 'PI',
      tariff: 2,
    },
    {
      origin: 'RN',
      destination: 'PR',
      tariff: 5,
    },
    {
      origin: 'RN',
      destination: 'RJ',
      tariff: 5,
    },
    {
      origin: 'RN',
      destination: 'RN',
      tariff: 4,
    },
    {
      origin: 'RN',
      destination: 'RO',
      tariff: 6,
    },
    {
      origin: 'RN',
      destination: 'RR',
      tariff: 6,
    },
    {
      origin: 'RN',
      destination: 'RS',
      tariff: 6,
    },
    {
      origin: 'RN',
      destination: 'SC',
      tariff: 5,
    },
    {
      origin: 'RN',
      destination: 'SE',
      tariff: 2,
    },
    {
      origin: 'RN',
      destination: 'SP',
      tariff: 5,
    },
    {
      origin: 'RN',
      destination: 'TO',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'AC',
      tariff: 1,
    },
    {
      origin: 'RO',
      destination: 'AL',
      tariff: 6,
    },
    {
      origin: 'RO',
      destination: 'AM',
      tariff: 2,
    },
    {
      origin: 'RO',
      destination: 'AP',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'BA',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'CE',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'DF',
      tariff: 4,
    },
    {
      origin: 'RO',
      destination: 'ES',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'RO',
      destination: 'MA',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'MG',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'MS',
      tariff: 3,
    },
    {
      origin: 'RO',
      destination: 'MT',
      tariff: 3,
    },
    {
      origin: 'RO',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'RO',
      destination: 'PB',
      tariff: 6,
    },
    {
      origin: 'RO',
      destination: 'PE',
      tariff: 6,
    },
    {
      origin: 'RO',
      destination: 'PI',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'PR',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'RJ',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'RN',
      tariff: 6,
    },
    {
      origin: 'RO',
      destination: 'RO',
      tariff: 4,
    },
    {
      origin: 'RO',
      destination: 'RR',
      tariff: 3,
    },
    {
      origin: 'RO',
      destination: 'RS',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'SC',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'SE',
      tariff: 6,
    },
    {
      origin: 'RO',
      destination: 'SP',
      tariff: 5,
    },
    {
      origin: 'RO',
      destination: 'TO',
      tariff: 5,
    },
    {
      origin: 'RR',
      destination: 'AC',
      tariff: 4,
    },
    {
      origin: 'RR',
      destination: 'AL',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'AM',
      tariff: 2,
    },
    {
      origin: 'RR',
      destination: 'AP',
      tariff: 4,
    },
    {
      origin: 'RR',
      destination: 'BA',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'CE',
      tariff: 5,
    },
    {
      origin: 'RR',
      destination: 'DF',
      tariff: 5,
    },
    {
      origin: 'RR',
      destination: 'ES',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'GO',
      tariff: 5,
    },
    {
      origin: 'RR',
      destination: 'MA',
      tariff: 4,
    },
    {
      origin: 'RR',
      destination: 'MG',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'MS',
      tariff: 5,
    },
    {
      origin: 'RR',
      destination: 'MT',
      tariff: 5,
    },
    {
      origin: 'RR',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'RR',
      destination: 'PB',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'PE',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'PI',
      tariff: 5,
    },
    {
      origin: 'RR',
      destination: 'PR',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'RJ',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'RN',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'RO',
      tariff: 3,
    },
    {
      origin: 'RR',
      destination: 'RR',
      tariff: 4,
    },
    {
      origin: 'RR',
      destination: 'RS',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'SC',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'SE',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'SP',
      tariff: 6,
    },
    {
      origin: 'RR',
      destination: 'TO',
      tariff: 4,
    },
    {
      origin: 'RS',
      destination: 'AC',
      tariff: 5,
    },
    {
      origin: 'RS',
      destination: 'AL',
      tariff: 5,
    },
    {
      origin: 'RS',
      destination: 'AM',
      tariff: 6,
    },
    {
      origin: 'RS',
      destination: 'AP',
      tariff: 6,
    },
    {
      origin: 'RS',
      destination: 'BA',
      tariff: 5,
    },
    {
      origin: 'RS',
      destination: 'CE',
      tariff: 6,
    },
    {
      origin: 'RS',
      destination: 'DF',
      tariff: 4,
    },
    {
      origin: 'RS',
      destination: 'ES',
      tariff: 4,
    },
    {
      origin: 'RS',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'RS',
      destination: 'MA',
      tariff: 6,
    },
    {
      origin: 'RS',
      destination: 'MG',
      tariff: 4,
    },
    {
      origin: 'RS',
      destination: 'MS',
      tariff: 3,
    },
    {
      origin: 'RS',
      destination: 'MT',
      tariff: 4,
    },
    {
      origin: 'RS',
      destination: 'PA',
      tariff: 6,
    },
    {
      origin: 'RS',
      destination: 'PB',
      tariff: 6,
    },
    {
      origin: 'RS',
      destination: 'PE',
      tariff: 6,
    },
    {
      origin: 'RS',
      destination: 'PI',
      tariff: 6,
    },
    {
      origin: 'RS',
      destination: 'PR',
      tariff: 1,
    },
    {
      origin: 'RS',
      destination: 'RJ',
      tariff: 3,
    },
    {
      origin: 'RS',
      destination: 'RN',
      tariff: 6,
    },
    {
      origin: 'RS',
      destination: 'RO',
      tariff: 5,
    },
    {
      origin: 'RS',
      destination: 'RR',
      tariff: 6,
    },
    {
      origin: 'RS',
      destination: 'RS',
      tariff: 3,
    },
    {
      origin: 'RS',
      destination: 'SC',
      tariff: 1,
    },
    {
      origin: 'RS',
      destination: 'SE',
      tariff: 5,
    },
    {
      origin: 'RS',
      destination: 'SP',
      tariff: 2,
    },
    {
      origin: 'RS',
      destination: 'TO',
      tariff: 5,
    },
    {
      origin: 'SC',
      destination: 'AC',
      tariff: 5,
    },
    {
      origin: 'SC',
      destination: 'AL',
      tariff: 5,
    },
    {
      origin: 'SC',
      destination: 'AM',
      tariff: 6,
    },
    {
      origin: 'SC',
      destination: 'AP',
      tariff: 6,
    },
    {
      origin: 'SC',
      destination: 'BA',
      tariff: 4,
    },
    {
      origin: 'SC',
      destination: 'CE',
      tariff: 6,
    },
    {
      origin: 'SC',
      destination: 'DF',
      tariff: 3,
    },
    {
      origin: 'SC',
      destination: 'ES',
      tariff: 3,
    },
    {
      origin: 'SC',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'SC',
      destination: 'MA',
      tariff: 5,
    },
    {
      origin: 'SC',
      destination: 'MG',
      tariff: 3,
    },
    {
      origin: 'SC',
      destination: 'MS',
      tariff: 2,
    },
    {
      origin: 'SC',
      destination: 'MT',
      tariff: 4,
    },
    {
      origin: 'SC',
      destination: 'PA',
      tariff: 6,
    },
    {
      origin: 'SC',
      destination: 'PB',
      tariff: 5,
    },
    {
      origin: 'SC',
      destination: 'PE',
      tariff: 5,
    },
    {
      origin: 'SC',
      destination: 'PI',
      tariff: 5,
    },
    {
      origin: 'SC',
      destination: 'PR',
      tariff: 1,
    },
    {
      origin: 'SC',
      destination: 'RJ',
      tariff: 2,
    },
    {
      origin: 'SC',
      destination: 'RN',
      tariff: 5,
    },
    {
      origin: 'SC',
      destination: 'RO',
      tariff: 5,
    },
    {
      origin: 'SC',
      destination: 'RR',
      tariff: 6,
    },
    {
      origin: 'SC',
      destination: 'RS',
      tariff: 1,
    },
    {
      origin: 'SC',
      destination: 'SC',
      tariff: 4,
    },
    {
      origin: 'SC',
      destination: 'SE',
      tariff: 5,
    },
    {
      origin: 'SC',
      destination: 'SP',
      tariff: 1,
    },
    {
      origin: 'SC',
      destination: 'TO',
      tariff: 4,
    },
    {
      origin: 'SE',
      destination: 'AC',
      tariff: 6,
    },
    {
      origin: 'SE',
      destination: 'AL',
      tariff: 1,
    },
    {
      origin: 'SE',
      destination: 'AM',
      tariff: 5,
    },
    {
      origin: 'SE',
      destination: 'AP',
      tariff: 5,
    },
    {
      origin: 'SE',
      destination: 'BA',
      tariff: 1,
    },
    {
      origin: 'SE',
      destination: 'CE',
      tariff: 2,
    },
    {
      origin: 'SE',
      destination: 'DF',
      tariff: 3,
    },
    {
      origin: 'SE',
      destination: 'ES',
      tariff: 3,
    },
    {
      origin: 'SE',
      destination: 'GO',
      tariff: 4,
    },
    {
      origin: 'SE',
      destination: 'MA',
      tariff: 3,
    },
    {
      origin: 'SE',
      destination: 'MG',
      tariff: 3,
    },
    {
      origin: 'SE',
      destination: 'MS',
      tariff: 4,
    },
    {
      origin: 'SE',
      destination: 'MT',
      tariff: 5,
    },
    {
      origin: 'SE',
      destination: 'PA',
      tariff: 4,
    },
    {
      origin: 'SE',
      destination: 'PB',
      tariff: 1,
    },
    {
      origin: 'SE',
      destination: 'PE',
      tariff: 1,
    },
    {
      origin: 'SE',
      destination: 'PI',
      tariff: 4,
    },
    {
      origin: 'SE',
      destination: 'PR',
      tariff: 5,
    },
    {
      origin: 'SE',
      destination: 'RJ',
      tariff: 4,
    },
    {
      origin: 'SE',
      destination: 'RN',
      tariff: 2,
    },
    {
      origin: 'SE',
      destination: 'RO',
      tariff: 6,
    },
    {
      origin: 'SE',
      destination: 'RR',
      tariff: 6,
    },
    {
      origin: 'SE',
      destination: 'RS',
      tariff: 5,
    },
    {
      origin: 'SE',
      destination: 'SC',
      tariff: 5,
    },
    {
      origin: 'SE',
      destination: 'SE',
      tariff: 4,
    },
    {
      origin: 'SE',
      destination: 'SP',
      tariff: 4,
    },
    {
      origin: 'SE',
      destination: 'TO',
      tariff: 5,
    },
    {
      origin: 'SP',
      destination: 'AC',
      tariff: 5,
    },
    {
      origin: 'SP',
      destination: 'AL',
      tariff: 4,
    },
    {
      origin: 'SP',
      destination: 'AM',
      tariff: 4,
    },
    {
      origin: 'SP',
      destination: 'AP',
      tariff: 4,
    },
    {
      origin: 'SP',
      destination: 'BA',
      tariff: 2,
    },
    {
      origin: 'SP',
      destination: 'CE',
      tariff: 3,
    },
    {
      origin: 'SP',
      destination: 'DF',
      tariff: 1,
    },
    {
      origin: 'SP',
      destination: 'ES',
      tariff: 1,
    },
    {
      origin: 'SP',
      destination: 'GO',
      tariff: 1,
    },
    {
      origin: 'SP',
      destination: 'MA',
      tariff: 4,
    },
    {
      origin: 'SP',
      destination: 'MG',
      tariff: 1,
    },
    {
      origin: 'SP',
      destination: 'MS',
      tariff: 1,
    },
    {
      origin: 'SP',
      destination: 'MT',
      tariff: 2,
    },
    {
      origin: 'SP',
      destination: 'PA',
      tariff: 3,
    },
    {
      origin: 'SP',
      destination: 'PB',
      tariff: 4,
    },
    {
      origin: 'SP',
      destination: 'PE',
      tariff: 3,
    },
    {
      origin: 'SP',
      destination: 'PI',
      tariff: 4,
    },
    {
      origin: 'SP',
      destination: 'PR',
      tariff: 1,
    },
    {
      origin: 'SP',
      destination: 'RJ',
      tariff: 1,
    },
    {
      origin: 'SP',
      destination: 'RN',
      tariff: 4,
    },
    {
      origin: 'SP',
      destination: 'RO',
      tariff: 4,
    },
    {
      origin: 'SP',
      destination: 'RR',
      tariff: 5,
    },
    {
      origin: 'SP',
      destination: 'RS',
      tariff: 1,
    },
    {
      origin: 'SP',
      destination: 'SC',
      tariff: 1,
    },
    {
      origin: 'SP',
      destination: 'SE',
      tariff: 4,
    },
    {
      origin: 'SP',
      destination: 'SP',
      tariff: 1,
    },
    {
      origin: 'SP',
      destination: 'TO',
      tariff: 3,
    },
    {
      origin: 'TO',
      destination: 'AC',
      tariff: 5,
    },
    {
      origin: 'TO',
      destination: 'AL',
      tariff: 5,
    },
    {
      origin: 'TO',
      destination: 'AM',
      tariff: 4,
    },
    {
      origin: 'TO',
      destination: 'AP',
      tariff: 3,
    },
    {
      origin: 'TO',
      destination: 'BA',
      tariff: 4,
    },
    {
      origin: 'TO',
      destination: 'CE',
      tariff: 5,
    },
    {
      origin: 'TO',
      destination: 'DF',
      tariff: 1,
    },
    {
      origin: 'TO',
      destination: 'ES',
      tariff: 4,
    },
    {
      origin: 'TO',
      destination: 'GO',
      tariff: 1,
    },
    {
      origin: 'TO',
      destination: 'MA',
      tariff: 5,
    },
    {
      origin: 'TO',
      destination: 'MG',
      tariff: 3,
    },
    {
      origin: 'TO',
      destination: 'MS',
      tariff: 3,
    },
    {
      origin: 'TO',
      destination: 'MT',
      tariff: 3,
    },
    {
      origin: 'TO',
      destination: 'PA',
      tariff: 2,
    },
    {
      origin: 'TO',
      destination: 'PB',
      tariff: 5,
    },
    {
      origin: 'TO',
      destination: 'PE',
      tariff: 4,
    },
    {
      origin: 'TO',
      destination: 'PI',
      tariff: 5,
    },
    {
      origin: 'TO',
      destination: 'PR',
      tariff: 4,
    },
    {
      origin: 'TO',
      destination: 'RJ',
      tariff: 3,
    },
    {
      origin: 'TO',
      destination: 'RN',
      tariff: 5,
    },
    {
      origin: 'TO',
      destination: 'RO',
      tariff: 5,
    },
    {
      origin: 'TO',
      destination: 'RR',
      tariff: 4,
    },
    {
      origin: 'TO',
      destination: 'RS',
      tariff: 5,
    },
    {
      origin: 'TO',
      destination: 'SC',
      tariff: 4,
    },
    {
      origin: 'TO',
      destination: 'SE',
      tariff: 5,
    },
    {
      origin: 'TO',
      destination: 'SP',
      tariff: 3,
    },
    {
      origin: 'TO',
      destination: 'TO',
      tariff: 4,
    },
    ]);
  },
};
