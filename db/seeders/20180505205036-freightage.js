module.exports = {
  up: queryInterface => queryInterface.bulkInsert('Freightages', [{
    service: 'economic',
    tax: 'E1',
    additionalPrice: 2.24,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 11.85,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 12.69,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 13.37,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 15.98,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 17.06,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 18.24,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 19.23,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 20.32,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 21.36,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 21.98,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 22.42,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'E2',
    additionalPrice: 2.34,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 12.35,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 13.23,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 13.94,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 16.66,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 17.79,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 19.01,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 20.05,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 21.19,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 22.27,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 22.91,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 23.37,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'E3',
    additionalPrice: 2.36,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 12.48,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 13.37,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 14.08,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 16.83,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 17.97,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 19.21,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 20.26,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 21.4,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 22.49,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 23.15,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 23.61,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'E4',
    additionalPrice: 2.39,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 12.6,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 13.5,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 14.22,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 17,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 18.15,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 19.4,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 20.46,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 21.62,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 22.72,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 23.38,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 23.85,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'N1',
    additionalPrice: 3.27,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 14.1,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 15.11,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 16.6,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 19.84,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 21.19,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 22.67,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 25.1,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 27.71,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 30.19,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 31.68,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 32.74,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'N2',
    additionalPrice: 3.77,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 15.79,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 16.92,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 18.59,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 22.22,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 23.73,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 25.39,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 28.87,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 31.87,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 34.72,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 36.43,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 37.65,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'N3',
    additionalPrice: 4.3,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 17.63,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 18.89,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 20.75,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 24.8,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 26.49,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 28.34,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 32.94,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 36.37,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 39.62,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 41.58,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 42.97,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'N4',
    additionalPrice: 5.32,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 21.15,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 22.67,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 24.9,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 29.76,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 31.79,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 34.01,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 40.79,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 45.03,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 49.06,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 51.48,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 53.2,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'N5',
    additionalPrice: 6.55,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 25.38,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 27.2,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 29.88,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 35.71,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 38.14,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 40.81,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 50.2,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 55.42,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 60.38,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 63.36,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 65.48,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'N6',
    additionalPrice: 8.19,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 31.02,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 33.24,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 36.52,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 43.65,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 46.62,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 49.87,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 62.75,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 69.28,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 75.48,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 79.2,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 81.85,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'I1',
    additionalPrice: 4.07,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 15.1,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 16.11,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 17.6,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 20.84,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 25.19,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 26.67,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 31.1,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 33.71,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 38.19,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 39.68,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 40.74,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'I2',
    additionalPrice: 4.57,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 16.79,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 17.92,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 19.59,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 23.22,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 27.73,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 29.39,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 34.87,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 37.87,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 42.72,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 44.43,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 45.65,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'I3',
    additionalPrice: 5.8,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 25.63,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 26.89,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 28.75,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 32.8,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 37.49,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 39.34,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 45.94,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 49.37,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 54.62,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 56.58,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 57.97,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'I4',
    additionalPrice: 7.32,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 34.15,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 35.67,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 37.9,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 42.76,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 47.79,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 50.01,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 58.79,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 63.03,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 69.06,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 71.48,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 73.2,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'I5',
    additionalPrice: 8.95,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 42.38,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 44.2,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 46.88,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 52.71,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 58.14,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 60.81,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 72.2,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 77.42,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 84.38,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 87.36,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 89.48,
    },
    ]),
  },
  {
    service: 'economic',
    tax: 'I6',
    additionalPrice: 11.19,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 500,
      },
      price: 54.02,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 56.24,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 59.52,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 66.65,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 72.62,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 75.87,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 90.75,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 97.28,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 105.48,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 109.2,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 111.85,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'L1',
    additionalPrice: 1.58,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 6.38,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 7.15,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 7.66,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 9.62,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 10.73,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 11.59,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 12.53,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 13.31,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 14.15,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 14.96,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 15.44,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 15.79,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'L2',
    additionalPrice: 1.62,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 6.51,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 7.3,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 7.82,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 9.82,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 10.96,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 11.83,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 12.79,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 13.59,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 14.46,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 15.28,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 15.77,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 16.12,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'L3',
    additionalPrice: 1.65,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 6.65,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 7.45,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 7.99,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 10.03,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 11.18,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 12.08,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 13.06,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 13.87,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 14.76,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 15.6,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 16.1,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 16.46,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'L4',
    additionalPrice: 1.68,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 6.78,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 7.6,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 8.15,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 10.23,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 11.41,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 12.32,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 13.33,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 14.15,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 15.06,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 15.92,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 16.42,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 16.79,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'E1',
    additionalPrice: 2.98,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 12.83,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 13.29,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 14.82,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 16.35,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 17.87,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 19.66,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 21.19,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 22.9,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 24.52,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 26.22,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 27.93,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 29.82,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'E2',
    additionalPrice: 3.01,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 13.11,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 13.59,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 14.97,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 16.51,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 18.05,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 19.87,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 21.41,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 23.13,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 24.77,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 26.49,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 28.22,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 30.13,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'E3',
    additionalPrice: 3.04,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 13.4,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 13.88,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 15.12,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 16.69,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 18.24,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 20.07,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 21.63,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 23.37,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 25.03,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 26.76,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 28.5,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 30.43,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'E4',
    additionalPrice: 3.08,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 14.25,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 14.77,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 15.28,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 16.85,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 18.42,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 20.27,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 21.84,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 23.61,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 25.27,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 27.04,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 28.8,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 30.74,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'N1',
    additionalPrice: 5.85,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 19.48,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 20.3,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 21.11,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 25.45,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 29.71,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 34.06,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 37.59,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 41.21,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 45.48,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 49.82,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 54.17,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 58.52,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'N2',
    additionalPrice: 7.9,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 27.27,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 28.4,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 29.55,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 35.64,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 40.11,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 45.99,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 50.74,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 55.64,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 61.39,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 67.26,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 73.13,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 79,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'N3',
    additionalPrice: 11.12,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 35.06,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 36.52,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 37.99,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 45.81,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 56.45,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 64.72,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 71.42,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 78.3,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 86.4,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 94.66,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 102.92,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 111.18,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'N4',
    additionalPrice: 13.46,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 40.9,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 42.61,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 44.32,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 53.45,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 68.33,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 78.34,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 86.45,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 94.79,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 104.6,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 114.6,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 124.59,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 134.59,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'N5',
    additionalPrice: 16.97,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 48.69,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 50.73,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 52.77,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 63.63,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 86.16,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 98.79,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 109.01,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 119.52,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 131.88,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 144.48,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 157.09,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 169.69,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'N6',
    additionalPrice: 21.07,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 58.43,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 60.88,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 63.32,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 76.36,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 106.95,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 122.62,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 135.32,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 148.37,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 163.72,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 179.37,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 195.01,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 210.66,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'I1',
    additionalPrice: 8.31,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 28.5,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 29.32,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 30.13,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 37.75,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 44.47,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 48.82,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 60.55,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 64.17,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 68.44,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 74.42,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 78.77,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 83.12,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'I2',
    additionalPrice: 10.4,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 36.7,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 37.83,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 38.98,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 48.35,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 55.28,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 61.16,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 74.11,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 79.01,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 84.76,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 92.27,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 98.14,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 104.01,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'I3',
    additionalPrice: 13.62,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 44.49,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 45.95,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 47.42,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 58.52,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 71.62,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 79.89,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 94.79,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 101.67,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 109.77,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 119.67,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 127.93,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 136.19,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'I4',
    additionalPrice: 16,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 50.74,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 52.45,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 54.16,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 66.57,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 83.91,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 93.92,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 110.23,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 118.57,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 128.38,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 140.02,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 150.01,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 160.01,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'I5',
    additionalPrice: 19.59,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 59.35,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 61.39,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 63.43,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 77.57,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 102.56,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 115.19,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 133.61,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 144.12,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 156.48,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 170.72,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 183.33,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 195.93,
    },
    ]),
  },
  {
    service: 'express',
    tax: 'I6',
    additionalPrice: 23.77,
    values: JSON.stringify([{
      weight: {
        from: 0,
        to: 300,
      },
      price: 69.91,
    },
    {
      weight: {
        from: 301,
        to: 500,
      },
      price: 72.36,
    },
    {
      weight: {
        from: 501,
        to: 1000,
      },
      price: 74.8,
    },
    {
      weight: {
        from: 1001,
        to: 2000,
      },
      price: 91.12,
    },
    {
      weight: {
        from: 2001,
        to: 3000,
      },
      price: 124.17,
    },
    {
      weight: {
        from: 3001,
        to: 4000,
      },
      price: 139.84,
    },
    {
      weight: {
        from: 4001,
        to: 5000,
      },
      price: 160.74,
    },
    {
      weight: {
        from: 5001,
        to: 6000,
      },
      price: 173.79,
    },
    {
      weight: {
        from: 6001,
        to: 7000,
      },
      price: 189.14,
    },
    {
      weight: {
        from: 7001,
        to: 8000,
      },
      price: 206.43,
    },
    {
      weight: {
        from: 8001,
        to: 9000,
      },
      price: 222.07,
    },
    {
      weight: {
        from: 9001,
        to: 10000,
      },
      price: 237.72,
    },
    ]),
  },
  ]),

  down: queryInterface => queryInterface.bulkInsert('Freightages', [

  ]),
};
