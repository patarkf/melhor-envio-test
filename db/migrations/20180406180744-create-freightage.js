module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('Freightages', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    service: {
      type: Sequelize.STRING,
    },
    tax: {
      type: Sequelize.STRING,
    },
    values: {
      type: Sequelize.JSON,
    },
    additionalPrice: {
      type: Sequelize.DECIMAL,
    },
  }),
  down: queryInterface => queryInterface.dropTable('Freightages'),
};
