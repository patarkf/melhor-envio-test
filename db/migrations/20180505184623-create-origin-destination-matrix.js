module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('OriginDestinationMatrices', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    origin: {
      type: Sequelize.STRING,
    },
    destination: {
      type: Sequelize.STRING,
    },
    tariff: {
      type: Sequelize.INTEGER,
    },
  }),
  down: queryInterface => queryInterface.dropTable('OriginDestinationMatrices'),
};
