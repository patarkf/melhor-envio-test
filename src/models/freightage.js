module.exports = (sequelize, DataTypes) => {
  const Freightage = sequelize.define('Freightage', {
    service: DataTypes.STRING,
    tax: DataTypes.STRING,
    values: DataTypes.JSON,
    additionalPrice: DataTypes.DECIMAL,
  }, {
    timestamps: false,
  });

  return Freightage;
};
