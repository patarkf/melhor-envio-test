module.exports = (sequelize, DataTypes) => {
  const OriginDestinationMatrix = sequelize.define('OriginDestinationMatrix', {
    origin: DataTypes.STRING,
    destination: DataTypes.STRING,
    tariff: DataTypes.INTEGER,
  }, {
    timestamps: false,
  });

  return OriginDestinationMatrix;
};
