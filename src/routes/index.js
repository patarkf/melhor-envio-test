const express = require('express');

const calculationController = require('../controllers/calculationController');
const { catchErrors } = require('../handlers/errorHandler');
const { freightageFieldsValidator } = require('../validators/freightage');

const router = express.Router();

router.post('/calculate', freightageFieldsValidator, catchErrors(calculationController.calculate));

module.exports = router;
