const { check } = require('express-validator/check');
const { getLocationByZipCode } = require('../services/location');
const sizes = require('../../config/objectSizes');

/**
 * Validate all freightage fields before starting the calculation.
 */
exports.freightageFieldsValidator = [
  check('originLocation')
    .custom(async (originZipCode, { req }) => {
      const originLocation = await getLocationByZipCode(originZipCode);
      if (!originLocation) return false;
      req.body.originLocation = originLocation;

      return true;
    })
    .withMessage('Please, you must provide a valid origin zip code'),

  check('destinationLocation')
    .custom(async (destinationZipCode, { req }) => {
      const destinationLocation = await getLocationByZipCode(destinationZipCode);
      if (!destinationLocation) return false;
      req.body.destinationLocation = destinationLocation;

      return true;
    })
    .withMessage('Please, you must provide a valid destination zip code'),

  check('height')
    .exists()
    .isInt({
      min: sizes.height.min,
      max: sizes.height.max,
    })
    .withMessage(`Please, provide a value between ${sizes.height.min} and ${sizes.height.max}`),

  check('width')
    .exists()
    .isInt({
      min: sizes.width.min,
      max: sizes.width.max,
    })
    .withMessage(`Please, provide a value between ${sizes.width.min} and ${sizes.width.max}`),

  check('length')
    .exists()
    .isInt({
      min: sizes.length.min,
      max: sizes.length.max,
    })
    .withMessage(`Please, provide a value between ${sizes.length.min} and ${sizes.length.max}`),

  check('weight')
    .exists()
    .isFloat({
      min: sizes.weight.min,
      max: sizes.weight.max,
    })
    .withMessage(`Please, provide a value between ${sizes.weight.min} and ${sizes.weight.max}`),

  check('insuredAmount')
    .exists()
    .isInt({
      min: sizes.insuredAmount.min,
      max: sizes.insuredAmount.express.max,
    })
    .withMessage(`Min value R$${sizes.insuredAmount.min} and max R$${sizes.insuredAmount.economic.max} (economic) or R$${sizes.insuredAmount.express.max} (express)`),

  check('additionalServices.aknowledgeOfReceipt')
    .exists()
    .isBoolean()
    .withMessage('Please, provide a boolean'),

  check('additionalServices.ownHand')
    .exists()
    .isBoolean()
    .withMessage('Please, provide a boolean'),
];
