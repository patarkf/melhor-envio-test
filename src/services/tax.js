const { getTariffNumber, getTariffLetter } = require('./tariff');

/**
 * Get region tax -- which is a combination of letter and number (e.g.: "E3"),
 * based on origin and destination location.
 *
 * @param {string} originCity
 * @param {string} originUf
 * @param {string} destinationCity
 * @param {string} destinationUf
 *
 * @returns {Object}
 */
const getTax = async (originCity, originUf, destinationCity, destinationUf) => {
  const tariffNumber = await getTariffNumber(originUf, destinationUf);
  const tariffLetter = getTariffLetter(originCity, originUf, destinationCity, destinationUf);

  return {
    economicTax: `${tariffLetter.economic}${tariffNumber}`,
    expressTax: `${tariffLetter.express}${tariffNumber}`,
  };
};

module.exports = {
  getTax,
};
