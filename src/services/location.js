const fetch = require('node-fetch');
const capitalCities = require('../../config/capitalCities');
const { url } = require('../../config/locationApi');

/**
 * Get a location object through a third-party API by zip code.
 *
 * @param {any} zipCode
 *
 * @returns {any}
 */
const getLocationByZipCode = async (zipCode) => {
  if (!zipCode) return false;

  const response = await fetch(`${url}/${zipCode}`);
  const location = await response.json();

  return location.error ? false : location;
};

/**
 * Check if origin and destination cities are equal.
 *
 * @param {string} originCity
 * @param {string} destinationCity
 *
 * @returns {boolean}
 */
const isSameCity = (originCity, destinationCity) => originCity === destinationCity;

/**
 * Check if origin and destination states are equal.
 *
 * @param {string} originUf
 * @param {string} destinationUf
 *
 * @returns {boolean}
 */
const isSameState = (originUf, destinationUf) => originUf === destinationUf;

/**
 * Check if origin and destination are both capital cities, checking
 * through the capitalCities object.
 *
 * @param {string} originCity
 * @param {string} destinationCity
 *
 * @returns {boolean}
 */
const areBothCapitals = (originCity, destinationCity) => {
  const isOriginCapital = Object.values(capitalCities).indexOf(originCity) > -1;
  const isDestinationCapital = Object.values(capitalCities).indexOf(destinationCity) > -1;

  return isOriginCapital && isDestinationCapital;
};

module.exports = {
  getLocationByZipCode,
  isSameCity,
  isSameState,
  areBothCapitals,
};
