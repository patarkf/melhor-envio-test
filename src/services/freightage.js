const { getBiggerWeight } = require('./weight');
const { getTax } = require('./tax');
const { getPrice } = require('./price');
const {
  getPriceWithOwnHandTax,
  getPriceWithAknowledgeOfReceiptTax,
  getPriceWithInsuredAmountTax,
  getPriceWithInsuredAmountTaxByValuableObject,
} = require('./additionalTax');


/**
 * Calculate the freightage price and taxes based on location,
 * object weight, additional services and region taxes.
 *
 * @param {Object} freightage Contains information about object and additional services.
 *
 * @returns {Object} Contains both "express" and "economic" services prices
 */
const getCalculatedFreightage = async ({
  originLocation,
  destinationLocation,
  height,
  width,
  length,
  weight,
  insuredAmount,
  additionalServices,
}) => {
  const { city: originCity, uf: originUf } = originLocation;
  const { city: destinationCity, uf: destinationUf } = destinationLocation;
  const { economicTax, expressTax } = await getTax(originCity, originUf, destinationCity, destinationUf);

  const objectWeight = getBiggerWeight(width, height, length, weight);

  // Get freightage prices by service
  let economicPrice = await getPrice('economic', economicTax, objectWeight);
  let expressPrice = await getPrice('express', expressTax, objectWeight);

  // Add insured amount taxes
  economicPrice = getPriceWithInsuredAmountTax(economicPrice, insuredAmount);
  expressPrice = getPriceWithInsuredAmountTax(expressPrice, insuredAmount);

  // Add insured amount taxes by valuable object
  economicPrice = getPriceWithInsuredAmountTaxByValuableObject(economicPrice, objectWeight, insuredAmount);
  expressPrice = getPriceWithInsuredAmountTaxByValuableObject(expressPrice, objectWeight, insuredAmount);

  const { ownHand, aknowledgeOfReceipt } = additionalServices;

  // Add own hand taxes if service was requested
  if (ownHand) {
    economicPrice = getPriceWithOwnHandTax(economicPrice);
    expressPrice = getPriceWithOwnHandTax(expressPrice);
  }

  // Add own aknowledge of receipt taxes if service was requested
  if (aknowledgeOfReceipt) {
    economicPrice = getPriceWithAknowledgeOfReceiptTax(economicPrice);
    expressPrice = getPriceWithAknowledgeOfReceiptTax(expressPrice);
  }

  return [{
    service: 'economic',
    price: economicPrice,
  },
  {
    service: 'express',
    price: expressPrice,
  },
  ];
};

module.exports = {
  getCalculatedFreightage,
};
