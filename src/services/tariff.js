const { OriginDestinationMatrix } = require('../models');
const { economicLetters, expressLetters } = require('../../config/tariffLetters');
const { isSameCity, isSameState, areBothCapitals } = require('./location');

/**
 * Get region tariff number based on origin and destination states.
 *
 * @param {string} originUf
 * @param {string} destinationUf
 *
 * @returns {int}
 */
const getTariffNumber = async (originUf, destinationUf) => {
  const { tariff } = await OriginDestinationMatrix.findOne({
    where: {
      origin: originUf,
      destination: destinationUf,
    },
  });

  return tariff;
};

/**
 * Get region tariff letter (for both "economic" and "express" services)
 * based on location premises.
 *
 * @param {string} originCity
 * @param {string} originUf
 * @param {string} destinationCity
 * @param {string} destinationUf
 *
 * @returns {Object}
 */
const getTariffLetter = (originCity, originUf, destinationCity, destinationUf) => {
  if (isSameCity(originCity, destinationCity)) {
    return {
      economic: economicLetters.sameCity,
      express: expressLetters.sameCity,
    };
  }

  if (isSameState(originUf, destinationUf)) {
    return {
      economic: economicLetters.sameState,
      express: expressLetters.sameState,
    };
  }

  if (areBothCapitals(originCity, destinationCity)) {
    return {
      economic: economicLetters.capitals,
      express: expressLetters.capitals,
    };
  }

  return {
    economic: economicLetters.notCapitals,
    express: expressLetters.notCapitals,
  };
};

module.exports = {
  getTariffNumber,
  getTariffLetter,
};
