const { maxWeight, weightToVolumeRatio } = require('../../config/weights');

/**
 * Calculate the cubic weight based on width, height, and length.
 * If the cubic weight of an object is bigger than the real weight,
 * the cubic weight is returned, otherwise, the real weight is returned.
 *
 * @param {int} width
 * @param {int} height
 * @param {int} length
 * @param {int} weight
 *
 * @returns {int} Weight that will be used on freightage calculation.
 */
const getBiggerWeight = (width, height, length, weight) => {
  const volume = length * (width * height);
  const cubicWeight = Math.ceil(volume / weightToVolumeRatio);

  return cubicWeight > maxWeight ? cubicWeight : weight;
};

module.exports = {
  getBiggerWeight,
};
