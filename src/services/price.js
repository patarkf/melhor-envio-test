const { Freightage } = require('../models');

/**
 * Increase the final price value based on the extra weight price
 * and quantity.
 *
 * @param {double} price Price in R$
 * @param {double} additionalPricePerWeight Additional price tax in R$
 * @param {int} additionalWeight Object weight in kg
 *
 * @returns {double}
 */
const getPriceWithAdditionalWeightTax = (price, additionalPricePerWeight, additionalWeight) => {
  const priceWithAdditionalTax = price + (additionalPricePerWeight * additionalWeight);

  return Math.round(priceWithAdditionalTax * 100) / 100;
};

/**
 *
 * @param {Array} values Array containing all the weight ranges and prices
 *                      of a given tax (e.g.: "E1", "L3", etc.)
 * @param {double} additionalPrice Additional price per weight in R$
 * @param {int} objectWeight Total object weight in kg
 */
const getPriceByWeight = ({ values, additionalPrice }, objectWeight) => {
  let finalPrice;

  values.forEach(({ weight, price }) => {
    const initialWeight = weight.from / 1000;
    const finalWeight = weight.to / 1000;

    if (objectWeight >= initialWeight && objectWeight <= finalWeight) {
      finalPrice = price;
    } else if (objectWeight > finalWeight) {
      const additionalWeight = objectWeight - finalWeight;
      finalPrice = getPriceWithAdditionalWeightTax(price, additionalPrice, additionalWeight);
    }
  });

  return finalPrice;
};

/**
 * Get freightage on database based on service and region tax,
 * get its weight ranges, prices, then calculate the freightage
 * price based on object weight and the above info.
 *
 * @param {string} service Can be either "economic" or "express"
 * @param {string} tax Freightage tax name (e.g: "E3", "L3", etc)
 * @param {int} objectWeight Total object weight in kg
 */
const getPrice = async (service, tax, objectWeight) => {
  const freightage = await Freightage.findOne({
    where: {
      service,
      tax,
    },
  });

  return getPriceByWeight(freightage, objectWeight);
};

module.exports = {
  getPrice,
};
