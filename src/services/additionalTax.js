const {
  ownHandTax,
  aknowledgeOfReceiptTax,
  insuredAmountTax,
  insuredAmountByValuableObject,
} = require('../../config/additionalTaxes');

/**
 * Add own hand extra tax (in R$).
 *
 * @param {double} price
 *
 * @returns {double} Price with additional tax
 */
const getPriceWithOwnHandTax = price => price + ownHandTax;

/**
 * Add aknowledge of receipt extra tax (in %).
 *
 * @param {double} price
 *
 * @returns {double} Price with additional tax
 */
const getPriceWithAknowledgeOfReceiptTax = (price) => {
  const additionalTax = (price * aknowledgeOfReceiptTax) / 100;
  return Math.round((price + additionalTax) * 100) / 100;
};

/**
 * Add insured amount extra tax (in %)
 *
 * @param {double} price
 * @param {double} insuredAmount
 *
 * @returns {double} Price with additional tax
 */
const getPriceWithInsuredAmountTax = (price, insuredAmount) => {
  const additionalTax = (insuredAmount * insuredAmountTax) / 100;
  return Math.round((price + additionalTax) * 100) / 100;
};

/**
 * Add insured amount extra tax by valuable object (in %)
 *
 * @param {double} price
 * @param {int} weight
 * @param {double} insuredAmount
 *
 * @returns {double} Price with additional tax
 */
const getPriceWithInsuredAmountTaxByValuableObject = (price, weight, insuredAmount) => {
  const valuableObjectAmount = insuredAmount / weight;

  if (valuableObjectAmount > insuredAmountByValuableObject) {
    return getPriceWithInsuredAmountTax(price, insuredAmount);
  }

  return price;
};

module.exports = {
  getPriceWithOwnHandTax,
  getPriceWithAknowledgeOfReceiptTax,
  getPriceWithInsuredAmountTax,
  getPriceWithInsuredAmountTaxByValuableObject,
};
