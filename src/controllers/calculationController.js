const { validationResult } = require('express-validator/check');
const { matchedData } = require('express-validator/filter');
const { getCalculatedFreightage } = require('../services/freightage');

/**
 * Check if there is any validation errors, if not, start
 * the calculation process and return its  results.
 *
 * @param {*} req
 * @param {*} res
 */
const calculate = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: errors.mapped(),
    });
  }

  return res
    .status(200)
    .send(await getCalculatedFreightage(matchedData(req)));
};

module.exports = {
  calculate,
};
