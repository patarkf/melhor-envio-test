# Melhor Envio - Backend test

## Starting App

```
npm install
npm run db:init
npm start
```

By running these commands you will start the application and create the database structure in your app dir. Just open [http://localhost:4000](http://localhost:4000).

## Running Tests

Tests are most implemented with [Mocha](https://mochajs.org) and [Expect.js](https://github.com/Automattic/expect.js?files=1)

To test everything properly, just run `npm test`

## Environments

The app is based on three different environments: dev, test, and production. 

For dev and test environments, the app will use an SQLite database, which as mentioned before,
will be created in your app dir (it's just a binary file that can be deleted and
replaced any time). 

Since that for this project we use the database for read-only purposes, test and dev environments share the same database.

For production, we use MySQL with some environment variables which you can set to configure your own instance.

