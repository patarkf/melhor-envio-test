describe('models/index', () => {
  it('returns the freightage model', () => {
    expect(models.Freightage).to.be.ok();
  });

  it('returns the origin destination matrix model', () => {
    expect(models.OriginDestinationMatrix).to.be.ok();
  });
});
