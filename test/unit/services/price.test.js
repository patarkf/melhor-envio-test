describe('Services: Price', () => {
  describe('getPrice() function', async () => {
    it('should return the right price for economic service', async () => {
      const weight = 2;
      const service = 'economic';
      const tax = 'E3';

      const price = await priceService.getPrice(service, tax, weight);
      expect(price).to.eql(14.08);
    });

    it('should return the right price for express service', async () => {
      const weight = 5;
      const service = 'express';
      const tax = 'N4';

      const price = await priceService.getPrice(service, tax, weight);
      expect(price).to.eql(86.45);
    });
  });
});
