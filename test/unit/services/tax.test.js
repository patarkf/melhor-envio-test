describe('Services: Tax', () => {
  describe('getTax() function', async () => {
    it('should return the right region taxes', async () => {
      const originCity = 'PELOTAS';
      const originUf = 'RS';

      const destinationCity = 'CURITIBA';
      const destinationUf = 'PR';

      const expectedTaxes = {
        economicTax: 'I1',
        expressTax: 'I1',
      };

      const taxes = await taxService.getTax(originCity, originUf, destinationCity, destinationUf);
      expect(taxes).to.eql(expectedTaxes);
    });
  });
});
