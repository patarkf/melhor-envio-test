describe('Services: Tariff', () => {
  describe('getTariffNumber() function', async () => {
    it('should return right tariff number when both origin and destination are from the same state', async () => {
      const origin = 'RS';
      const destination = 'RS';

      const tariffNumber = await tariffService.getTariffNumber(origin, destination);

      expect(tariffNumber).to.eql(3);
    });

    it('should return right tariff number when both origin and destination from differente states', async () => {
      const origin = 'RS';
      const destination = 'PR';

      const tariffNumber = await tariffService.getTariffNumber(origin, destination);

      expect(tariffNumber).to.eql(1);
    });
  });

  describe('getTariffLetter() function', async () => {
    it('should return right letters (for economic and express) when cities are equal', async () => {
      const originCity = 'PELOTAS';
      const originUf = 'RS';

      const destinationCity = 'PELOTAS';
      const destinationUf = 'RS';

      const tariffLetter = await tariffService.getTariffLetter(
        originCity,
        originUf,
        destinationCity,
        destinationUf,
      );

      expect(tariffLetter.economic).to.eql('E');
      expect(tariffLetter.express).to.eql('L');
    });

    it('should return right letters (for economic and express) when states are equal', async () => {
      const originCity = 'PELOTAS';
      const originUf = 'RS';

      const destinationCity = 'RIO GRANDE';
      const destinationUf = 'RS';

      const tariffLetter = await tariffService.getTariffLetter(
        originCity,
        originUf,
        destinationCity,
        destinationUf,
      );

      expect(tariffLetter.economic).to.eql('E');
      expect(tariffLetter.express).to.eql('E');
    });

    it('should return right letters (for economic and express) when both cities are capitals', async () => {
      const originCity = 'PORTO ALEGRE';
      const originUf = 'RS';

      const destinationCity = 'RIO DE JANEIRO';
      const destinationUf = 'RJ';

      const tariffLetter = await tariffService.getTariffLetter(
        originCity,
        originUf,
        destinationCity,
        destinationUf,
      );

      expect(tariffLetter.economic).to.eql('N');
      expect(tariffLetter.express).to.eql('N');
    });

    it('should return right letters (for economic and express) when cities are not capitals', async () => {
      const originCity = 'PELOTAS';
      const originUf = 'RS';

      const destinationCity = 'RIO DE JANEIRO';
      const destinationUf = 'RJ';

      const tariffLetter = await tariffService.getTariffLetter(
        originCity,
        originUf,
        destinationCity,
        destinationUf,
      );

      expect(tariffLetter.economic).to.eql('I');
      expect(tariffLetter.express).to.eql('I');
    });
  });
});
