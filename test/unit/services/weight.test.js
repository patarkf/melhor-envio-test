describe('Services: Weight', () => {
  describe('getBiggerWeight() function', () => {
    it('should return the cubic weight when it is bigger than the max weight (10kg)', () => {
      const width = 38;
      const height = 40;
      const length = 45;
      const weight = 8;

      const finalWeight = weightService.getBiggerWeight(width, height, length, weight);

      expect(finalWeight).to.eql(12);
    });

    it('should return the real weight when it is smaller than the max weight (10kg)', () => {
      const width = 18;
      const height = 20;
      const length = 25;
      const weight = 8;

      const finalWeight = weightService.getBiggerWeight(width, height, length, weight);

      expect(finalWeight).to.eql(8);
    });
  });
});
