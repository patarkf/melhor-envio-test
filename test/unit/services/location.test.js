describe('Services: Location', () => {
  describe('isSameCity() function', async () => {
    it('should return true when both cities are equal', async () => {
      const origin = 'PELOTAS';
      const destination = 'PELOTAS';

      const isSameCity = locationService.isSameCity(origin, destination);
      expect(isSameCity).to.be.ok();
    });

    it('should return false when cities are different', async () => {
      const origin = 'PELOTAS';
      const destination = 'RIO GRANDE';

      const isSameCity = locationService.isSameCity(origin, destination);
      expect(isSameCity).to.not.be.ok();
    });
  });

  describe('isSameState() function', async () => {
    it('should return true when both states are equal', async () => {
      const origin = 'RS';
      const destination = 'RS';

      const isSameState = locationService.isSameState(origin, destination);
      expect(isSameState).to.be.ok();
    });

    it('should return false when states are different', async () => {
      const origin = 'RS';
      const destination = 'RJ';

      const isSameState = locationService.isSameState(origin, destination);
      expect(isSameState).to.not.be.ok();
    });
  });

  describe('areBothCapitals() function', async () => {
    it('should return true when both cities are capitals', async () => {
      const origin = 'PORTO ALEGRE';
      const destination = 'FLORIANÓPOLIS';

      const areBothCapitals = locationService.areBothCapitals(origin, destination);
      expect(areBothCapitals).to.be.ok();
    });

    it('should return false when not both cities are capitals', async () => {
      const origin = 'PORTO ALEGRE';
      const destination = 'PELOTAS';

      const areBothCapitals = locationService.areBothCapitals(origin, destination);
      expect(areBothCapitals).to.not.be.ok();
    });
  });

  describe('getLocationByZipCode() function', async () => {
    it('should return correct location when valid zipcode is provided', async () => {
      const zipCode = '20010-020';

      const expectedLocation = {
        cep: '20010020',
        logradouro: 'SÃO JOSÉ',
        tipo: 'RUA',
        bairro: 'CENTRO',
        cidade: 'RIO DE JANEIRO',
        uf: 'RJ',
        numero: '',
        ibge: '3304557',
        ddd: '21',
        latitude: null,
        longitude: null,
      };

      const location = await locationService.getLocationByZipCode(zipCode);
      expect(location).to.eql(expectedLocation);
    });

    it('should return an error message when a not valid zipcode is provided', async () => {
      const zipCode = 'invalid-zipcode';

      const location = await locationService.getLocationByZipCode(zipCode);
      expect(location).to.not.be.ok();
    });
  });
});
