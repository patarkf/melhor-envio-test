describe('Services: Additional Tax', () => {
  describe('getPriceWithOwnHandTax() function', () => {
    it('should increase price using own hand tax correctly', () => {
      const price = 50.30;
      const increasedPrice = additionalTaxService.getPriceWithOwnHandTax(price);

      expect(increasedPrice).to.eql(53.30);
    });
  });

  describe('getPriceWithAknowledgeOfReceiptTax() function', () => {
    it('should increase price using aknowlodge of receipt tax correctly', () => {
      const price = 50.30;
      const increasedPrice = additionalTaxService.getPriceWithAknowledgeOfReceiptTax(price);

      expect(increasedPrice).to.eql(52.82);
    });
  });

  describe('getPriceWithInsuredAmountTax() function', () => {
    it('should increase price using aknowlodge of receipt tax correctly', () => {
      const price = 50.30;
      const insuredAmount = 19.35;

      const increasedPrice = additionalTaxService.getPriceWithInsuredAmountTax(price, insuredAmount);

      expect(increasedPrice).to.eql(50.49);
    });
  });

  describe('getPriceWithInsuredAmountTaxByValuableObject() function', () => {
    it('should increase price using aknowlodge of receipt tax correctly', () => {
      const price = 50.30;
      const insuredAmount = 4000;
      const weight = 1;

      const increasedPrice = additionalTaxService.getPriceWithInsuredAmountTaxByValuableObject(
        price,
        weight,
        insuredAmount,
      );

      expect(increasedPrice).to.eql(90.3);
    });
  });
});
