/**
 * @property {int} ownHandTax Value in R$
 * @property {int} aknowledgeOfReceiptTax Value in %
 * @property {int} aknowledgeOfReceiptTax Value in %
 * @property {int} aknowledgeOfReceiptTax Value in R$
 */
module.exports = {
  ownHandTax: 3,
  aknowledgeOfReceiptTax: 5,
  insuredAmountTax: 1,
  insuredAmountByValuableObject: 3000,
};
