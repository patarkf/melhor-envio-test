/**
 * List of min and max object sizes.
 *
 * @property {Object} height Accepts min and max value in cm (integer)
 * @property {Object} width Accepts min and max value in cm (integer)
 * @property {Object} length Accepts min and max value in cm (integer)
 * @property {Object} weight Accepts min and max value in kg (float or integer)
 * @property {Object} insuredAmount Accepts min and max value in R$ (integer)
 */
module.exports = {
  height: {
    min: 2,
    max: 105,
  },
  width: {
    min: 11,
    max: 105,
  },
  length: {
    min: 16,
    max: 105,
  },
  weight: {
    min: 1,
    max: 30,
  },
  insuredAmount: {
    min: 17,
    economic: {
      max: 3000,
    },
    express: {
      max: 10000,
    },
  },
};
