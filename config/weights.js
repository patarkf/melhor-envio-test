/**
 * List of weight specifications.
 *
 * @property {int} maxWeight Defines the max weight size in kg
 * @property {int} weightToVolumeRatio Defines the weight x volume ratio
 */
module.exports = {
  maxWeight: 10,
  weightToVolumeRatio: 6000,
};
