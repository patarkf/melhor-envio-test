/**
 * Defines each service letters based on region tax:
 *
 * - "L": when origin and destination are in the same city;
 * - "E": when origin and destination are in the same state;
 * - "N": when origin and destination are both capitals from different states;
 * - "I": when origin and destination are both from different states (but are not capitals)
 *
 * @property {Object} economicLetters
 * @property {Object} expressLetters
 */
module.exports = {
  economicLetters: {
    sameCity: 'E',
    sameState: 'E',
    capitals: 'N',
    notCapitals: 'I',
  },
  expressLetters: {
    sameCity: 'L',
    sameState: 'E',
    capitals: 'N',
    notCapitals: 'I',
  },
};
