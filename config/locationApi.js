/**
 * @property {string} url URL used to retrieve location info based on zip code.
 */
module.exports = {
  url: 'https://location.melhorenvio.com.br',
};
