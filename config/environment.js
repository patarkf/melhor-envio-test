const { Op } = require('sequelize');

module.exports = {
  development: {
    dialect: 'sqlite',
    storage: './db.development.sqlite',
    operatorsAliases: Op,
  },
  test: {
    dialect: 'sqlite',
    storage: './db.development.sqlite',
    logging: false,
    operatorsAliases: Op,
  },
  production: {
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    host: process.env.MYSQL_HOST,
    dialect: 'mysql',
    operatorsAliases: Op,
  },
};
